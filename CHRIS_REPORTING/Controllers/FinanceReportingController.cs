﻿using CHRIS_REPORTING.Common;
using CHRIS_REPORTING.ReportModels;
using System;
using System.IO;
using System.Net.Http;
using System.Web.Http;
using System.Net;
using System.Net.Http.Headers;

namespace CHRIS_REPORTING.Controllers
{
    public class FinanceReportingController : ApiController
    {
        Basereporting BRT = new Basereporting();
        Stream ReportFile = new MemoryStream();

        [HttpPost]
        [Route("api/FinanceReporting/FinancePerson/")]
        public HttpResponseMessage FinancePerson(HttpRequestMessage requestmessage)
        {
            var data = requestmessage.Content.ReadAsStringAsync().Result;
            dynamic RequestData = System.Web.Helpers.Json.Decode(data);

            FNREP08Model thisRptClass = new FNREP08Model();
            thisRptClass.P_NO = RequestData.P_NO;
            ReportFile = BRT.GetReportWithSubReportCustom(thisRptClass, "FNREP08");

            if (ReportFile != null)
                return ReturnFileAsMessage("FNREP08_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf", ReportFile);
            else
            {
                ResonseMessageFromCHRISReporting returnmessage = new ResonseMessageFromCHRISReporting();
                returnmessage.code = "500";
                returnmessage.description = "Error While Generating File " + DateTime.Now;
                return Request.CreateResponse(HttpStatusCode.OK, returnmessage);
            }
        }

        [HttpPost]
        [Route("api/FinanceReporting/FinanceLiquidation/")]
        public HttpResponseMessage FinanceLiquidation(HttpRequestMessage requestmessage)
        {
            var data = requestmessage.Content.ReadAsStringAsync().Result;
            dynamic RequestData = System.Web.Helpers.Json.Decode(data);

                FNREP09Model thisRptClass = new FNREP09Model();
                thisRptClass.branch = RequestData.Branch;
                thisRptClass.w_seg = RequestData.Segment;
                if (RequestData.FromDate != null)
                    thisRptClass.fdate = Convert.ToDateTime(RequestData.FromDate);
                if (RequestData.ToDate != null)
                    thisRptClass.tdate = Convert.ToDateTime(RequestData.ToDate);
                thisRptClass.IO_FLAG = RequestData.Flag;

                ReportFile = BRT.GetReportWithSubReportCustom(thisRptClass, "FNREP09");

            if (ReportFile != null)
                return ReturnFileAsMessage("FNREP09_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf", ReportFile);
            else
            {
                ResonseMessageFromCHRISReporting returnmessage = new ResonseMessageFromCHRISReporting();
                returnmessage.code = "500";
                returnmessage.description = "Error While Generating File " + DateTime.Now;
                return Request.CreateResponse(HttpStatusCode.OK, returnmessage);
            }
        }

        [HttpPost]
        [Route("api/FinanceReporting/FinanceOutstanding/")]
        public HttpResponseMessage FinanceOutstanding(HttpRequestMessage requestmessage)
        {
            var data = requestmessage.Content.ReadAsStringAsync().Result;
            dynamic RequestData = System.Web.Helpers.Json.Decode(data);

            FNREP10Model thisRptClass = new FNREP10Model();
            thisRptClass.BRANCH = RequestData.Branch;

            ReportFile = BRT.GetReportWithSubReportCustom(thisRptClass, "FNREP10");

            if (ReportFile != null)
                return ReturnFileAsMessage("FNREP10" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf", ReportFile);
            else
            {
                ResonseMessageFromCHRISReporting returnmessage = new ResonseMessageFromCHRISReporting();
                returnmessage.code = "500";
                returnmessage.description = "Error While Generating File " + DateTime.Now;
                return Request.CreateResponse(HttpStatusCode.OK, returnmessage);
            }
        }

        [HttpPost]
        [Route("api/FinanceReporting/FinanceBalance/")]
        public HttpResponseMessage FinanceBalance(HttpRequestMessage requestmessage)
        {
            var data = requestmessage.Content.ReadAsStringAsync().Result;
            dynamic RequestData = System.Web.Helpers.Json.Decode(data);

            FNREP01Model thisRptClass = new FNREP01Model();
            thisRptClass.w_branch = RequestData.Branch;
            if (RequestData.Date != null)
                thisRptClass.wmdate = Convert.ToDateTime(RequestData.Date);
            thisRptClass.seg = RequestData.Segment;

            ReportFile = BRT.GetReportWithSubReportCustom(thisRptClass, "FNREP01");

            if (ReportFile != null)
                return ReturnFileAsMessage("FNREP01" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf", ReportFile);
            else
            {
                ResonseMessageFromCHRISReporting returnmessage = new ResonseMessageFromCHRISReporting();
                returnmessage.code = "500";
                returnmessage.description = "Error While Generating File " + DateTime.Now;
                return Request.CreateResponse(HttpStatusCode.OK, returnmessage);
            }
        }

        [HttpPost]
        [Route("api/FinanceReporting/MonthlyFinance/")]
        public HttpResponseMessage MonthlyFinance(HttpRequestMessage requestmessage)
        {
            var data = requestmessage.Content.ReadAsStringAsync().Result;
            dynamic RequestData = System.Web.Helpers.Json.Decode(data);

            FNREP02Model thisRptClass = new FNREP02Model();
            thisRptClass.W_BRANCH = RequestData.Branch;
            thisRptClass.W_SEG = RequestData.Segment;

            ReportFile = BRT.GetReportWithSubReportCustom(thisRptClass, "FNREP02");

            if (ReportFile != null)
                return ReturnFileAsMessage("FNREP02" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf", ReportFile);
            else
            {
                ResonseMessageFromCHRISReporting returnmessage = new ResonseMessageFromCHRISReporting();
                returnmessage.code = "500";
                returnmessage.description = "Error While Generating File " + DateTime.Now;
                return Request.CreateResponse(HttpStatusCode.OK, returnmessage);
            }
        }

        [HttpPost]
        [Route("api/FinanceReporting/FinanceLedger/")]
        public HttpResponseMessage FinanceLedger(HttpRequestMessage requestmessage)
        {
            var data = requestmessage.Content.ReadAsStringAsync().Result;
            dynamic RequestData = System.Web.Helpers.Json.Decode(data);

            FNREP03Model thisRptClass = new FNREP03Model();
            thisRptClass.fin_no = RequestData.FiananceNo;

            ReportFile = BRT.GetReportWithSubReportCustom(thisRptClass, "FNREP03");

            if (ReportFile != null)
                return ReturnFileAsMessage("FNREP03" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf", ReportFile);
            else
            {
                ResonseMessageFromCHRISReporting returnmessage = new ResonseMessageFromCHRISReporting();
                returnmessage.code = "500";
                returnmessage.description = "Error While Generating File " + DateTime.Now;
                return Request.CreateResponse(HttpStatusCode.OK, returnmessage);
            }
        }

        [HttpPost]
        [Route("api/FinanceReporting/MCOReport/")]
        public HttpResponseMessage MCOReport(HttpRequestMessage requestmessage)
        {
            var data = requestmessage.Content.ReadAsStringAsync().Result;
            dynamic RequestData = System.Web.Helpers.Json.Decode(data);

            MCOREP2Model thisRptClass = new MCOREP2Model();
            if (RequestData.Balance != null)
                thisRptClass.GL_BALANCE = Convert.ToDecimal(RequestData.Balance);
            else
                thisRptClass.GL_BALANCE = null;
            thisRptClass.W_BRANCH = RequestData.Branch;
            thisRptClass.W_SEG = RequestData.Segment;

            ReportFile = BRT.GetReportWithSubReportCustom(thisRptClass, "MCOREP2");

            if (ReportFile != null)
                return ReturnFileAsMessage("MCOREP2" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf", ReportFile);
            else
            {
                ResonseMessageFromCHRISReporting returnmessage = new ResonseMessageFromCHRISReporting();
                returnmessage.code = "500";
                returnmessage.description = "Error While Generating File " + DateTime.Now;
                return Request.CreateResponse(HttpStatusCode.OK, returnmessage);
            }
        }
        
        public HttpResponseMessage ReturnFileAsMessage(string filename, Stream fileStream)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.BadRequest);
            byte[] buffer = new byte[0];
            //get buffer
            using (MemoryStream ms = new MemoryStream())
            {
                fileStream.CopyTo(ms);
                buffer = ms.ToArray();
            }
            //content length for use in header
            var contentLength = buffer.Length;
            //200
            //successful
            var statuscode = HttpStatusCode.OK;
            response = Request.CreateResponse(statuscode);
            response.Content = new StreamContent(new MemoryStream(buffer));
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            response.Content.Headers.ContentLength = contentLength;
            ContentDispositionHeaderValue contentDisposition = null;
            if (ContentDispositionHeaderValue.TryParse("inline; filename=" + filename + "", out contentDisposition))
            {
                response.Content.Headers.ContentDisposition = contentDisposition;
            }
            return response;

        }
    }
}
