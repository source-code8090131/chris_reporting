﻿using CHRIS_REPORTING.Common;
using CHRIS_REPORTING.ReportModels;
using System;
using System.IO;
using System.Net.Http;
using System.Web.Http;
using System.Net;
using System.Net.Http.Headers;

namespace CHRIS_REPORTING.Controllers
{
    public class GratuityReportingController : ApiController
    {
        Basereporting BRT = new Basereporting();
        Stream ReportFile = new MemoryStream();

        [HttpPost]
        [Route("api/GratuityReporting/IndividualGratuityReports/")]
        public HttpResponseMessage IndividualGratuityReports(HttpRequestMessage requestmessage)
        {
            var data = requestmessage.Content.ReadAsStringAsync().Result;
            dynamic RequestData = System.Web.Helpers.Json.Decode(data);

            GRR02Model thisRptClass = new GRR02Model();
            if (RequestData.FPNO != null)
                thisRptClass.w_pfrom = Convert.ToInt32(RequestData.FPNO);
            else
                thisRptClass.w_pfrom = null;
            if (RequestData.TPNO != null)
                thisRptClass.w_pto = Convert.ToInt32(RequestData.TPNO);
            else
                thisRptClass.w_pto = null;
            thisRptClass.w_seg = RequestData.Segment;
            thisRptClass.w_brn = RequestData.Branch;
            thisRptClass.w_dept = RequestData.Department;

            ReportFile = BRT.GetReportWithSubReportCustom(thisRptClass, "GRR02");

            if (ReportFile != null)
                return ReturnFileAsMessage("GRR02_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf", ReportFile);
            else
            {
                ResonseMessageFromCHRISReporting returnmessage = new ResonseMessageFromCHRISReporting();
                returnmessage.code = "500";
                returnmessage.description = "Error While Generating File " + DateTime.Now;
                return Request.CreateResponse(HttpStatusCode.OK, returnmessage);
            }
        }

        public HttpResponseMessage ReturnFileAsMessage(string filename, Stream fileStream)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.BadRequest);
            byte[] buffer = new byte[0];
            //get buffer
            using (MemoryStream ms = new MemoryStream())
            {
                fileStream.CopyTo(ms);
                buffer = ms.ToArray();
            }
            //content length for use in header
            var contentLength = buffer.Length;
            //200
            //successful
            var statuscode = HttpStatusCode.OK;
            response = Request.CreateResponse(statuscode);
            response.Content = new StreamContent(new MemoryStream(buffer));
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            response.Content.Headers.ContentLength = contentLength;
            ContentDispositionHeaderValue contentDisposition = null;
            if (ContentDispositionHeaderValue.TryParse("inline; filename=" + filename + "", out contentDisposition))
            {
                response.Content.Headers.ContentDisposition = contentDisposition;
            }
            return response;

        }
    }
}
