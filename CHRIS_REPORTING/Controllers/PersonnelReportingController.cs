﻿using CHRIS_REPORTING.Common;
using CHRIS_REPORTING.ReportModels;
using System;
using System.IO;
using System.Net.Http;
using System.Web.Http;
using System.Net;
using System.Net.Http.Headers;

namespace CHRIS_REPORTING.Controllers
{
    public class PersonnelReportingController : ApiController
    {
        Basereporting BRT = new Basereporting();
        Stream ReportFile = new MemoryStream();

        [HttpGet]
        [Route("api/CheckAPI/Index/")]
        public System.Collections.Generic.IEnumerable<string> Get()
        {
            return new string[] { "CHRIS Reporting API is running. " + DateTime.Now };
        }

        [HttpPost]
        [Route("api/PersonnelReporting/PersonnelReportForAll/")]
        public HttpResponseMessage PersonnelReportForAll(HttpRequestMessage requestmessage)
        {
            var data = requestmessage.Content.ReadAsStringAsync().Result;
            dynamic RequestData = System.Web.Helpers.Json.Decode(data);

            PRREP46Model thisRptClass = new PRREP46Model();
            thisRptClass.BRANCH = RequestData.Branch;
            if (RequestData.From_Date != null)
                thisRptClass.frm_date = Convert.ToDateTime(RequestData.From_Date);
            else
                thisRptClass.frm_date = null;
            if (RequestData.To_Date != null)
                thisRptClass.to_date = Convert.ToDateTime(RequestData.To_Date);
            else
                thisRptClass.to_date = null;
            thisRptClass.w_desig = RequestData.Desig;
            thisRptClass.w_seg = RequestData.Segment;
            thisRptClass.pno_start = RequestData.FromPrNo;
            thisRptClass.pno_end = RequestData.EndPrNo;
            ReportFile = BRT.GetReportWithSubReportCustom(thisRptClass, "PRREP46_1");

            if (ReportFile != null)
                return ReturnFileAsMessage("PRREP46_1_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf", ReportFile);
            else
            {
                ResonseMessageFromCHRISReporting returnmessage = new ResonseMessageFromCHRISReporting();
                returnmessage.code = "500";
                returnmessage.description = "Error While Generating File " + DateTime.Now;
                return Request.CreateResponse(HttpStatusCode.OK, returnmessage);
            }
        }

        [HttpPost]
        [Route("api/PersonnelReporting/PersonnelOnPagerReport/")]
        public HttpResponseMessage PersonnelOnPagerReport(HttpRequestMessage requestmessage)
        {
            var data = requestmessage.Content.ReadAsStringAsync().Result;
            dynamic RequestData = System.Web.Helpers.Json.Decode(data);

            CITI_EMP2Model thisRptClass = new CITI_EMP2Model();
            thisRptClass.BRN = RequestData.Branch;
            thisRptClass.SPNO = RequestData.FromPNo;
            thisRptClass.TPNO = RequestData.ToPNo;
            thisRptClass.SEG = RequestData.Segment;
            thisRptClass.DPT = RequestData.Department;
            thisRptClass.LEV = RequestData.Level;
            thisRptClass.P_DESIG = RequestData.Desig;
            thisRptClass.P_GRP = RequestData.Group;
            ReportFile = BRT.GetReportWithSubReportCustom(thisRptClass, "CITI_EMP2");

            if (ReportFile != null)
                return ReturnFileAsMessage("CITI_EMP2_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf", ReportFile);
            else
            {
                ResonseMessageFromCHRISReporting returnmessage = new ResonseMessageFromCHRISReporting();
                returnmessage.code = "500";
                returnmessage.description = "Error While Generating File " + DateTime.Now;
                return Request.CreateResponse(HttpStatusCode.OK, returnmessage);
            }
        }

        [HttpPost]
        [Route("api/PersonnelReporting/NewHiresHead_CountAnalysis/")]
        public HttpResponseMessage NewHiresHead_CountAnalysis(HttpRequestMessage requestmessage)
        {
            var data = requestmessage.Content.ReadAsStringAsync().Result;
            dynamic RequestData = System.Web.Helpers.Json.Decode(data);

            PRREP52Model thisRptClass = new PRREP52Model();
            thisRptClass.w_brn = RequestData.Branch;
            thisRptClass.w_seg = RequestData.Segment;
            thisRptClass.w_group = RequestData.Group;
            if (RequestData.FromDate != null)
                thisRptClass.sdate = Convert.ToDateTime(RequestData.FromDate);
            else
                thisRptClass.sdate = null;
            if (RequestData.EndDate != null)
                thisRptClass.edate = Convert.ToDateTime(RequestData.EndDate);
            else
                thisRptClass.edate = null;
            ReportFile = BRT.GetReportWithSubReportCustom(thisRptClass, "PRREP52");

            if (ReportFile != null)
                return ReturnFileAsMessage("PRREP52_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf", ReportFile);
            else
            {
                ResonseMessageFromCHRISReporting returnmessage = new ResonseMessageFromCHRISReporting();
                returnmessage.code = "500";
                returnmessage.description = "Error While Generating File " + DateTime.Now;
                return Request.CreateResponse(HttpStatusCode.OK, returnmessage);
            }
        }

        [HttpPost]
        [Route("api/PersonnelReporting/NewInduction/")]
        public HttpResponseMessage NewInduction(HttpRequestMessage requestmessage)
        {
            var data = requestmessage.Content.ReadAsStringAsync().Result;
            dynamic RequestData = System.Web.Helpers.Json.Decode(data);
            PRREP06Model thisRptClass = new PRREP06Model();
            if (RequestData.FromDate != null)
                thisRptClass.S_DATE = Convert.ToDateTime(RequestData.FromDate);
            else
                thisRptClass.S_DATE = null;
            if (RequestData.EndDate != null)
                thisRptClass.E_DATE = Convert.ToDateTime(RequestData.EndDate);
            else
                thisRptClass.E_DATE = null;
            thisRptClass.W_SEG = RequestData.Segment;
            thisRptClass.W_BRN = RequestData.Branch;
            thisRptClass.W_DESIG = RequestData.Desig;
            thisRptClass.W_GROUP = RequestData.Group;
            ReportFile = BRT.GetReportWithSubReportCustom(thisRptClass, "PRREP06");

            if (ReportFile != null)
                return ReturnFileAsMessage("PRREP06_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf", ReportFile);
            else
            {
                ResonseMessageFromCHRISReporting returnmessage = new ResonseMessageFromCHRISReporting();
                returnmessage.code = "500";
                returnmessage.description = "Error While Generating File " + DateTime.Now;
                return Request.CreateResponse(HttpStatusCode.OK, returnmessage);
            }
        }

        [HttpPost]
        [Route("api/PersonnelReporting/ReportForAllEmployeesCurrentDeptWise/")]
        public HttpResponseMessage ReportForAllEmployeesCurrentDeptWise(HttpRequestMessage requestmessage)
        {
            var data = requestmessage.Content.ReadAsStringAsync().Result;
            dynamic RequestData = System.Web.Helpers.Json.Decode(data);
            PRREP0001Model thisRptClass = new PRREP0001Model();
            thisRptClass.W_BRN = RequestData.Branch;
            thisRptClass.W_SEG = RequestData.Segment;
            thisRptClass.W_DEPT = RequestData.Department;
            thisRptClass.W_GROUP = RequestData.Group;
            thisRptClass.W_DESIG = RequestData.Desig;
            thisRptClass.W_LEVEL = RequestData.Level;
            ReportFile = BRT.GetReportWithSubReportCustom(thisRptClass, "PRREP0001");

            if (ReportFile != null)
                return ReturnFileAsMessage("PRREP0001_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf", ReportFile);
            else
            {
                ResonseMessageFromCHRISReporting returnmessage = new ResonseMessageFromCHRISReporting();
                returnmessage.code = "500";
                returnmessage.description = "Error While Generating File " + DateTime.Now;
                return Request.CreateResponse(HttpStatusCode.OK, returnmessage);
            }
        }

        [HttpPost]
        [Route("api/PersonnelReporting/EmployeesForProbation/")]
        public HttpResponseMessage EmployeesForProbation(HttpRequestMessage requestmessage)
        {
            var data = requestmessage.Content.ReadAsStringAsync().Result;
            dynamic RequestData = System.Web.Helpers.Json.Decode(data);
            PRREP03Model thisRptClass = new PRREP03Model();
            thisRptClass.W_BRN = RequestData.Branch;
            thisRptClass.w_seg = RequestData.Segment;
            thisRptClass.w_desig = RequestData.Desig;
            thisRptClass.w_level = RequestData.Level;
            ReportFile = BRT.GetReportWithSubReportCustom(thisRptClass, "PRREP03");

            if (ReportFile != null)
                return ReturnFileAsMessage("PRREP03_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf", ReportFile);
            else
            {
                ResonseMessageFromCHRISReporting returnmessage = new ResonseMessageFromCHRISReporting();
                returnmessage.code = "500";
                returnmessage.description = "Error While Generating File " + DateTime.Now;
                return Request.CreateResponse(HttpStatusCode.OK, returnmessage);
            }
        }

        public HttpResponseMessage ReturnFileAsMessage(string filename, Stream fileStream)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.BadRequest);
            byte[] buffer = new byte[0];
            //get buffer
            using (MemoryStream ms = new MemoryStream())
            {
                fileStream.CopyTo(ms);
                buffer = ms.ToArray();
            }
            //content length for use in header
            var contentLength = buffer.Length;
            //200
            //successful
            var statuscode = HttpStatusCode.OK;
            response = Request.CreateResponse(statuscode);
            response.Content = new StreamContent(new MemoryStream(buffer));
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            response.Content.Headers.ContentLength = contentLength;
            ContentDispositionHeaderValue contentDisposition = null;
            if (ContentDispositionHeaderValue.TryParse("inline; filename=" + filename + "", out contentDisposition))
            {
                response.Content.Headers.ContentDisposition = contentDisposition;
            }
            return response;

        }
    }
}
