﻿using CHRIS_REPORTING.Common;
using CHRIS_REPORTING.ReportModels;
using System;
using System.IO;
using System.Net.Http;
using System.Web.Http;
using System.Net;
using System.Net.Http.Headers;

namespace CHRIS_REPORTING.Controllers
{
    public class PayrollReportingController : ApiController
    {
        Basereporting BRT = new Basereporting();
        Stream ReportFile = new MemoryStream();

        [HttpPost]
        [Route("api/PayrollReporting/PeriodicReportForIncPromotion/")]
        public HttpResponseMessage PeriodicReportForIncPromotion(HttpRequestMessage requestmessage)
        {
            var data = requestmessage.Content.ReadAsStringAsync().Result;
            dynamic RequestData = System.Web.Helpers.Json.Decode(data);

            PY001Model thisRptClass = new PY001Model();
            thisRptClass.w_brn = RequestData.Branch;
            if (RequestData.From_Date != null)
                thisRptClass.date1 = Convert.ToDateTime(RequestData.From_Date);
            else
                thisRptClass.date1 = null;
            if (RequestData.To_Date != null)
                thisRptClass.date2 = Convert.ToDateTime(RequestData.To_Date);
            else
                thisRptClass.date2 = null;
            ReportFile = BRT.GetReportWithSubReportCustom(thisRptClass, "PY001");

            if (ReportFile != null)
                return ReturnFileAsMessage("PY001_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf", ReportFile);
            else
            {
                ResonseMessageFromCHRISReporting returnmessage = new ResonseMessageFromCHRISReporting();
                returnmessage.code = "500";
                returnmessage.description = "Error While Generating File " + DateTime.Now;
                return Request.CreateResponse(HttpStatusCode.OK, returnmessage);
            }
        }

        [HttpPost]
        [Route("api/PayrollReporting/MonthlyStaffPaymentListings/")]
        public HttpResponseMessage MonthlyStaffPaymentListings(HttpRequestMessage requestmessage)
        {
            var data = requestmessage.Content.ReadAsStringAsync().Result;
            dynamic RequestData = System.Web.Helpers.Json.Decode(data);

            PY002Model thisRptClass = new PY002Model();
            thisRptClass.W_BRANCH = RequestData.Branch;
            if (RequestData.Month != null)
                thisRptClass.W_MONTH = Convert.ToInt32(RequestData.Month);
            else
                thisRptClass.W_MONTH = null;
            if (RequestData.Year != null)
                thisRptClass.YEAR = Convert.ToInt32(RequestData.Year);
            else
                thisRptClass.YEAR = null;
            thisRptClass.c_flag = RequestData.Flag;
            thisRptClass.cat = RequestData.Category;
            thisRptClass.W_SEG = RequestData.Segment;
            ReportFile = BRT.GetReportWithSubReportCustom(thisRptClass, "PY002");

            if (ReportFile != null)
                return ReturnFileAsMessage("PY002_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf", ReportFile);
            else
            {
                ResonseMessageFromCHRISReporting returnmessage = new ResonseMessageFromCHRISReporting();
                returnmessage.code = "500";
                returnmessage.description = "Error While Generating File " + DateTime.Now;
                return Request.CreateResponse(HttpStatusCode.OK, returnmessage);
            }
        }

        [HttpPost]
        [Route("api/PayrollReporting/ProvidentFundReport/")]
        public HttpResponseMessage ProvidentFundReport(HttpRequestMessage requestmessage)
        {
            var data = requestmessage.Content.ReadAsStringAsync().Result;
            dynamic RequestData = System.Web.Helpers.Json.Decode(data);

            PY003Model thisRptClass = new PY003Model();
            thisRptClass.BRANCH = RequestData.Branch;
            thisRptClass.W_SEG = RequestData.Segment;
            if (RequestData.Month != null)
                thisRptClass.W_MONTH = Convert.ToInt32(RequestData.Month);
            else
                thisRptClass.W_MONTH = null;
            if (RequestData.Year != null)
                thisRptClass.WYEAR = Convert.ToInt32(RequestData.Year);
            else
                thisRptClass.WYEAR = null;
            thisRptClass.CAT = RequestData.Category;
            ReportFile = BRT.GetReportWithSubReportCustom(thisRptClass, "PY003");

            if (ReportFile != null)
                return ReturnFileAsMessage("PY003_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf", ReportFile);
            else
            {
                ResonseMessageFromCHRISReporting returnmessage = new ResonseMessageFromCHRISReporting();
                returnmessage.code = "500";
                returnmessage.description = "Error While Generating File " + DateTime.Now;
                return Request.CreateResponse(HttpStatusCode.OK, returnmessage);
            }
        }

        [HttpPost]
        [Route("api/PayrollReporting/SalarySettlementOfficer/")]
        public HttpResponseMessage SalarySettlementOfficer(HttpRequestMessage requestmessage)
        {
            var data = requestmessage.Content.ReadAsStringAsync().Result;
            dynamic RequestData = System.Web.Helpers.Json.Decode(data);

            PY011_BModel thisRptClass = new PY011_BModel();
            thisRptClass.BRANCH = RequestData.Branch;
            thisRptClass.W_SEG = RequestData.Segment;
            if (RequestData.Month != null)
                thisRptClass.MONTH = Convert.ToInt32(RequestData.Month);
            else
                thisRptClass.MONTH = null;
            if (RequestData.Year != null)
                thisRptClass.WYEAR = Convert.ToInt32(RequestData.Year);
            else
                thisRptClass.WYEAR = null;
            thisRptClass.CAT = RequestData.Category;
            thisRptClass.W_DEPT = RequestData.Department;
            thisRptClass.FPNO = RequestData.FPNO;
            thisRptClass.TPNO = RequestData.TPNO;
            thisRptClass.MODE = RequestData.Mode;
            ReportFile = BRT.GetReportWithSubReportCustom(thisRptClass, "PY011_B");

            if (ReportFile != null)
                return ReturnFileAsMessage("PY011_B_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf", ReportFile);
            else
            {
                ResonseMessageFromCHRISReporting returnmessage = new ResonseMessageFromCHRISReporting();
                returnmessage.code = "500";
                returnmessage.description = "Error While Generating File " + DateTime.Now;
                return Request.CreateResponse(HttpStatusCode.OK, returnmessage);
            }
        }

        [HttpPost]
        [Route("api/PayrollReporting/MonthlyTaxReport/")]
        public HttpResponseMessage MonthlyTaxReport(HttpRequestMessage requestmessage)
        {
            var data = requestmessage.Content.ReadAsStringAsync().Result;
            dynamic RequestData = System.Web.Helpers.Json.Decode(data);

            MONTHLY_TAXModel thisRptClass = new MONTHLY_TAXModel();
            thisRptClass.p_branch = RequestData.Branch;
            if (RequestData.Month != null)
                thisRptClass.p_month = Convert.ToDateTime(RequestData.Month);
            else
                thisRptClass.p_month = null;
           
            ReportFile = BRT.GetReportWithSubReportCustom(thisRptClass, "MONTHLY_TAX");

            if (ReportFile != null)
                return ReturnFileAsMessage("MONTHLY_TAX_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf", ReportFile);
            else
            {
                ResonseMessageFromCHRISReporting returnmessage = new ResonseMessageFromCHRISReporting();
                returnmessage.code = "500";
                returnmessage.description = "Error While Generating File " + DateTime.Now;
                return Request.CreateResponse(HttpStatusCode.OK, returnmessage);
            }
        }

        [HttpPost]
        [Route("api/PayrollReporting/EmployeeListForSalary/")]
        public HttpResponseMessage EmployeeListForSalary(HttpRequestMessage requestmessage)
        {
            var data = requestmessage.Content.ReadAsStringAsync().Result;
            dynamic RequestData = System.Web.Helpers.Json.Decode(data);

            PRREP53Model thisRptClass = new PRREP53Model();
            ReportFile = BRT.GetReportWithSubReportCustom(thisRptClass, "PRREP53");

            if (ReportFile != null)
                return ReturnFileAsMessage("PRREP53_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf", ReportFile);
            else
            {
                ResonseMessageFromCHRISReporting returnmessage = new ResonseMessageFromCHRISReporting();
                returnmessage.code = "500";
                returnmessage.description = "Error While Generating File " + DateTime.Now;
                return Request.CreateResponse(HttpStatusCode.OK, returnmessage);
            }
        }

        [HttpPost]
        [Route("api/PayrollReporting/ATR_DETAIL/")]
        public HttpResponseMessage ATR_DETAIL(HttpRequestMessage requestmessage)
        {
            var data = requestmessage.Content.ReadAsStringAsync().Result;
            dynamic RequestData = System.Web.Helpers.Json.Decode(data);

            ATR_DETAILModel thisRptClass = new ATR_DETAILModel();
            ReportFile = BRT.GetReportWithSubReportCustom(thisRptClass, "ATR_DETAIL");

            if (ReportFile != null)
                return ReturnFileAsMessage("ATR_DETAIL_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf", ReportFile);
            else
            {
                ResonseMessageFromCHRISReporting returnmessage = new ResonseMessageFromCHRISReporting();
                returnmessage.code = "500";
                returnmessage.description = "Error While Generating File " + DateTime.Now;
                return Request.CreateResponse(HttpStatusCode.OK, returnmessage);
            }
        }

        [HttpPost]
        [Route("api/PayrollReporting/ATRIncentiveAllow/")]
        public HttpResponseMessage ATRIncentiveAllow(HttpRequestMessage requestmessage)
        {
            var data = requestmessage.Content.ReadAsStringAsync().Result;
            dynamic RequestData = System.Web.Helpers.Json.Decode(data);

            ATR_INCENTIVE_ALLOWModel thisRptClass = new ATR_INCENTIVE_ALLOWModel();
            thisRptClass.P_BRANCH = RequestData.Branch;
            thisRptClass.P_CAT = RequestData.Category;
            thisRptClass.P_ALLOW = RequestData.Allow;
            thisRptClass.P_EMP_NO = RequestData.EmployeeNo;

            ReportFile = BRT.GetReportWithSubReportCustom(thisRptClass, "ATR_INCENTIVE_ALLOW");

            if (ReportFile != null)
                return ReturnFileAsMessage("ATR_INCENTIVE_ALLOW_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf", ReportFile);
            else
            {
                ResonseMessageFromCHRISReporting returnmessage = new ResonseMessageFromCHRISReporting();
                returnmessage.code = "500";
                returnmessage.description = "Error While Generating File " + DateTime.Now;
                return Request.CreateResponse(HttpStatusCode.OK, returnmessage);
            }
        }

        public HttpResponseMessage ReturnFileAsMessage(string filename, Stream fileStream)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.BadRequest);
            byte[] buffer = new byte[0];
            //get buffer
            using (MemoryStream ms = new MemoryStream())
            {
                fileStream.CopyTo(ms);
                buffer = ms.ToArray();
            }
            //content length for use in header
            var contentLength = buffer.Length;
            //200
            //successful
            var statuscode = HttpStatusCode.OK;
            response = Request.CreateResponse(statuscode);
            response.Content = new StreamContent(new MemoryStream(buffer));
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            response.Content.Headers.ContentLength = contentLength;
            ContentDispositionHeaderValue contentDisposition = null;
            if (ContentDispositionHeaderValue.TryParse("inline; filename=" + filename + "", out contentDisposition))
            {
                response.Content.Headers.ContentDisposition = contentDisposition;
            }
            return response;

        }
    }
}
