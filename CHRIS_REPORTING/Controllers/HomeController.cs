﻿using CHRIS_REPORTING.Common;
using CHRIS_REPORTING.ReportModels;
using CHRIS_REPORTING.Services;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Net;
using System.Net.Http.Headers;

namespace CHRIS_REPORTING.Controllers
{
    public class HomeController : ApiController
    {
        Stream FinalFile;
        //public ActionResult Index()
        //{
        //    ViewBag.Title = "Home Page";
        //    return View();
        //}

        [HttpPost]
        [Route("api/ChrisReportings/Index/")]
        public HttpResponseMessage Index(HttpRequestMessage requestmessage)
        {
            var data = requestmessage.Content.ReadAsStringAsync().Result;
            dynamic RequestData = System.Web.Helpers.Json.Decode(data);
            //ViewBag.Title = "Home Page";
            if (RequestData.ReportName == "FNREP08")
            {
                FNREP08Model thisRptClass = new FNREP08Model();
                thisRptClass.P_NO = RequestData.P_NO;
                ShowReportWithSubReportCustom(thisRptClass, RequestData.ReportName);
            }
            else if (RequestData.ReportName == "FNREP09")
            {
                FNREP09Model thisRptClass = new FNREP09Model();
                thisRptClass.branch = RequestData.Branch;
                thisRptClass.w_seg = RequestData.Segment;
                if (RequestData.FromDate != null)
                    thisRptClass.fdate = Convert.ToDateTime(RequestData.FromDate);
                if (RequestData.ToDate != null)
                    thisRptClass.tdate = Convert.ToDateTime(RequestData.ToDate);
                thisRptClass.IO_FLAG = RequestData.Flag;

                ShowReportWithSubReportCustom(thisRptClass, RequestData.ReportName);
            }
            else if (RequestData.ReportName == "FNREP10")
            {
                FNREP10Model thisRptClass = new FNREP10Model();
                thisRptClass.BRANCH = RequestData.Branch;

                ShowReportWithSubReportCustom(thisRptClass, RequestData.ReportName);
            }
            else if (RequestData.ReportName == "FNREP01")
            {
                FNREP01Model thisRptClass = new FNREP01Model();
                thisRptClass.w_branch = RequestData.Branch;
                if (RequestData.Date != null)
                    thisRptClass.wmdate = Convert.ToDateTime(RequestData.Date);
                thisRptClass.seg = RequestData.Segment;

                ShowReportWithSubReportCustom(thisRptClass, RequestData.ReportName);
            }
            else if (RequestData.ReportName == "FNREP02")
            {
                FNREP02Model thisRptClass = new FNREP02Model();
                thisRptClass.W_BRANCH = RequestData.Branch;
                thisRptClass.W_SEG = RequestData.Segment;
                ShowReportWithSubReportCustom(thisRptClass, RequestData.ReportName);
            }

            if (FinalFile != null)
                return ReturnFileAsMessage(RequestData.ReportName +  "_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf", FinalFile);
            else
            {
                ResonseMessageFromCHRISReporting returnmessage = new ResonseMessageFromCHRISReporting();
                returnmessage.code = "500";
                returnmessage.description = "Error While Generating File " + DateTime.Now;
                return Request.CreateResponse(HttpStatusCode.OK, returnmessage);
            }
        }
        public void ShowReportWithSubReportCustom<T>(T entity, string rptFileName) where T : class
        {
            CrystalDecisions.CrystalReports.Engine.ReportDocument Temp;
            string m_rptFilePath = Path.Combine(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath, "Reports\\" + rptFileName + ".rpt");
            ConnectionBean bean = new ConnectionBean();
            SQLManager.SetConnectionBean(bean);

            string m_SPName = "CHRIS_RPT_SP_" + rptFileName;
            DataTable dataTable = new DataTable();
            dataTable.TableName = m_SPName;

            dataTable = SQLManager.GetSPParams(m_SPName);
            dataTable.Rows.Add();
            ReportDocument crDoc = new ReportDocument();
            crDoc.Load(m_rptFilePath);
            string[] Credentials = { bean.DbName, bean.M_ServerName };
            ConnectionInfo conn = new ConnectionInfo();
            conn.DatabaseName = bean.DbName;
            conn.ServerName = bean.M_ServerName;
            DoCrystalReportLogin(crDoc, Credentials);
            DataSet ds = new DataSet();
            ds.Tables.Clear();
            ds.Tables.Add(dataTable);
            foreach (var prop in typeof(T).GetProperties())
            {
                if (dataTable.Columns.Contains(prop.Name))
                {
                    var value = prop.GetValue(entity);
                    dataTable.Rows[0][prop.Name] = value != null ? value : DBNull.Value;
                }
            }
            Result result = SQLManager.SelectDataSet(ds);
            if (result.isSuccessful)
            {
                if (result.dstResult.Tables.Count > 0)
                {
                    foreach (DataTable table in result.dstResult.Tables)
                    {
                        string tableName = table.TableName;
                        string CCrtableName = table.TableName;
                        bool tableExists = false;
                        // Check if the table exists in the report's database tables collection
                        foreach (Table crTable in crDoc.Database.Tables)
                        {
                            string ReportSimpleTablename = crTable.Name.Substring(0, crTable.Name.IndexOf(";"));
                            if (ReportSimpleTablename == tableName)
                            {
                                tableExists = true;
                                CCrtableName = crTable.Name;
                                break;
                            }
                        }

                        if (tableExists)
                        {
                            Table crTable = crDoc.Database.Tables[CCrtableName];
                            // Set the data source for the Crystal Report table
                            crTable.SetDataSource(table);
                        }
                        else
                        {
                            // Handle if the table doesn't exist in the report
                            // You might log or handle this situation according to your requirements
                            Console.WriteLine($"Table {tableName} not found in the report.");
                        }
                    }
                    //crDoc.SetDataSource(new[] { result.dstResult });
                    crDoc.Refresh();
                    //Stream ReportFileStream = crDoc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    //string DateTimeNow = DateTime.Now.ToString("HHmmss");
                    //string SavingPath = Path.Combine(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath, "TesOutputFolder\\" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf");
                    //using (var fileStream = new FileStream(SavingPath, FileMode.Create, FileAccess.Write))
                    //{
                    //    ReportFileStream.CopyTo(fileStream);
                    //}
                    //crDoc.Close();
                    //crDoc.Dispose();


                    Stream stream = crDoc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    FinalFile = stream;
                }
            }

        }
        public bool DoCrystalReportLogin(CrystalDecisions.CrystalReports.Engine.ReportDocument rptDoc, string[] Credentials)
        {

            TableLogOnInfo conInfo = new TableLogOnInfo();

            bool testReturn = true;
            conInfo.ConnectionInfo.DatabaseName = Credentials[0];
            conInfo.ConnectionInfo.ServerName = Credentials[1];
            conInfo.ConnectionInfo.IntegratedSecurity = true;

            foreach (CrystalDecisions.CrystalReports.Engine.Table tbl in rptDoc.Database.Tables)
            {
                tbl.ApplyLogOnInfo(conInfo);
                if (!tbl.TestConnectivity())
                {
                    testReturn = false;
                    break;
                }

                if (tbl.Location.IndexOf(".") > 0)
                {
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1);
                }
                else
                {
                    tbl.Location = tbl.Location;
                }

            }

            if (testReturn && rptDoc.ReportDefinition.ReportObjects.Count > 0)
            {
                int index, count, counter;
                SubreportObject mySubReportObject;
                ReportDocument mySubRepDoc;

                for (index = 0; index <= rptDoc.ReportDefinition.Sections.Count - 1; index++)
                {
                    for (count = 0; count <= rptDoc.ReportDefinition.Sections[index].ReportObjects.Count - 1; count++)
                    {
                        Section rptSection = rptDoc.ReportDefinition.Sections[index];
                        if (rptSection.ReportObjects[count].Kind == ReportObjectKind.SubreportObject)
                        {
                            mySubReportObject = (SubreportObject)rptSection.ReportObjects[count];
                            mySubRepDoc = mySubReportObject.OpenSubreport(mySubReportObject.SubreportName);
                            for (counter = 0; counter <= mySubRepDoc.Database.Tables.Count - 1; counter++)
                            {
                                mySubRepDoc.Database.Tables[counter].ApplyLogOnInfo(conInfo);
                                if (!mySubRepDoc.Database.Tables[counter].TestConnectivity())
                                {
                                    testReturn = false;
                                    break;
                                }

                            }

                        }
                    }

                }

            }

            return testReturn;

        }
        public HttpResponseMessage ReturnFileAsMessage(string filename, Stream fileStream)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.BadRequest);
            byte[] buffer = new byte[0];
            //get buffer
            using (MemoryStream ms = new MemoryStream())
            {
                fileStream.CopyTo(ms);
                buffer = ms.ToArray();
            }
            //content length for use in header
            var contentLength = buffer.Length;
            //200
            //successful
            var statuscode = HttpStatusCode.OK;
            response = Request.CreateResponse(statuscode);
            response.Content = new StreamContent(new MemoryStream(buffer));
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            response.Content.Headers.ContentLength = contentLength;
            ContentDispositionHeaderValue contentDisposition = null;
            if (ContentDispositionHeaderValue.TryParse("inline; filename=" + filename + "", out contentDisposition))
            {
                response.Content.Headers.ContentDisposition = contentDisposition;
            }
            return response;

        }
    }
}
