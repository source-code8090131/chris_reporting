﻿using CHRIS_REPORTING.Common;
using CHRIS_REPORTING.ReportModels;
using CHRIS_REPORTING.Services;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace CHRIS_REPORTING.Controllers
{
    public class ChrisReportingController : ApiController
    {
        Stream FinalFile;

        [HttpGet]
        [Route("api/ChrisReporting/Index/")]
        public HttpResponseMessage Get()
        {
            ResonseMessageFromCHRISReporting message = new ResonseMessageFromCHRISReporting();
            message.code = "200";
            message.description = "CHRIS Reporting Service is up and running " + DateTime.Now;
            return Request.CreateResponse(HttpStatusCode.OK, message);
        }
        [HttpPost]
        [Route("api/ChrisReporting/Index/")]
        public HttpResponseMessage Post(HttpRequestMessage requestmessage)
        {
            string message = "";
            FNREP08Model thisRptClass = new FNREP08Model();
            thisRptClass.P_NO = "410";

            Basereporting rptt = new Basereporting();
            ShowReportWithSubReportCustom(thisRptClass, "FNREP08");
            if (FinalFile != null)
                return ReturnFileAsMessage("FNREP08_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf", FinalFile);
            else
            {
                ResonseMessageFromCHRISReporting returnmessage = new ResonseMessageFromCHRISReporting();
                returnmessage.code = "500";
                returnmessage.description = "Error While Generating FIle " + DateTime.Now;
                return Request.CreateResponse(HttpStatusCode.OK, returnmessage);
            }
        }


        public HttpResponseMessage ReturnFileAsMessage(string filename, Stream fileStream)
        {
            var stream = fileStream;

            using (MemoryStream ms = new MemoryStream())
            {
                stream.CopyTo(ms);
                var result = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new ByteArrayContent(ms.ToArray())
                };
                result.Content.Headers.ContentDisposition =
                new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = filename
                };
                result.Content.Headers.ContentType =
                    new MediaTypeHeaderValue("application/octet-stream");

                return result;
            }
            // processing the stream.
        }

        public void ShowReportWithSubReportCustom<T>(T entity, string rptFileName) where T : class
        {
            CrystalDecisions.CrystalReports.Engine.ReportDocument Temp;
            string m_rptFilePath = Path.Combine(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath, "Reports\\" + rptFileName + ".rpt");
            ConnectionBean bean = new ConnectionBean();
            SQLManager.SetConnectionBean(bean);

            string m_SPName = "CHRIS_RPT_SP_" + rptFileName;
            DataTable dataTable = new DataTable();
            dataTable.TableName = m_SPName;

            dataTable = SQLManager.GetSPParams(m_SPName);
            dataTable.Rows.Add();
            ReportDocument crDoc = new ReportDocument();
            crDoc.Load(m_rptFilePath);
            string[] Credentials = { bean.DbName, bean.M_ServerName };
            ConnectionInfo conn = new ConnectionInfo();
            conn.DatabaseName = bean.DbName;
            conn.ServerName = bean.M_ServerName;
            DoCrystalReportLogin(crDoc, Credentials);
            DataSet ds = new DataSet();
            ds.Tables.Clear();
            ds.Tables.Add(dataTable);
            foreach (var prop in typeof(T).GetProperties())
            {
                if (dataTable.Columns.Contains(prop.Name))
                {
                    var value = prop.GetValue(entity);
                    dataTable.Rows[0][prop.Name] = value != null ? value : DBNull.Value;
                }
            }
            Result result = SQLManager.SelectDataSet(ds);
            if (result.isSuccessful)
            {
                if (result.dstResult.Tables.Count > 0)
                {
                    if (result.dstResult.Tables[0].Rows.Count > 0)
                    {

                        foreach (DataTable table in result.dstResult.Tables)
                        {
                            string tableName = table.TableName;
                            string CCrtableName = table.TableName;
                            bool tableExists = false;
                            // Check if the table exists in the report's database tables collection
                            foreach (Table crTable in crDoc.Database.Tables)
                            {
                                string ReportSimpleTablename = crTable.Name.Substring(0, crTable.Name.IndexOf(";"));
                                if (ReportSimpleTablename == tableName)
                                {
                                    tableExists = true;
                                    CCrtableName = crTable.Name;
                                    break;
                                }
                            }

                            if (tableExists)
                            {
                                Table crTable = crDoc.Database.Tables[CCrtableName];
                                // Set the data source for the Crystal Report table
                                crTable.SetDataSource(table);
                            }
                            else
                            {
                                // Handle if the table doesn't exist in the report
                                // You might log or handle this situation according to your requirements
                                Console.WriteLine($"Table {tableName} not found in the report.");
                            }
                        }
                        //crDoc.SetDataSource(new[] { result.dstResult });
                        crDoc.Refresh();
                          Stream ReportFileStream = crDoc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                        if(ReportFileStream == null)
                            FinalFile = new MemoryStream();
                        FinalFile = ReportFileStream;
                        //string DateTimeNow = DateTime.Now.ToString("HHmmss");
                        //string SavingPath = Path.Combine(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath, "TesOutputFolder\\" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf");
                        //using (var fileStream = new FileStream(SavingPath, FileMode.Create, FileAccess.Write))
                        //{
                        //    ReportFileStream.CopyTo(fileStream);
                        //}
                        //crDoc.Close();
                        //crDoc.Dispose();
                        //if (FinalFile == null)
                        //    FinalFile = new MemoryStream();
                        //crDoc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat).CopyTo(FinalFile);
                        //FinalFile.Seek(0, SeekOrigin.Begin);
                        //return File(stream, "application/pdf", "FNREP08_" + DateTime.Now.ToLongDateString() + ".pdf");
                    }
                }
            }

        }
        public bool DoCrystalReportLogin(CrystalDecisions.CrystalReports.Engine.ReportDocument rptDoc, string[] Credentials)
        {

            TableLogOnInfo conInfo = new TableLogOnInfo();

            bool testReturn = true;
            conInfo.ConnectionInfo.DatabaseName = Credentials[0];
            conInfo.ConnectionInfo.ServerName = Credentials[1];
            conInfo.ConnectionInfo.IntegratedSecurity = true;

            foreach (CrystalDecisions.CrystalReports.Engine.Table tbl in rptDoc.Database.Tables)
            {
                tbl.ApplyLogOnInfo(conInfo);
                if (!tbl.TestConnectivity())
                {
                    testReturn = false;
                    break;
                }

                if (tbl.Location.IndexOf(".") > 0)
                {
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1);
                }
                else
                {
                    tbl.Location = tbl.Location;
                }

            }

            if (testReturn && rptDoc.ReportDefinition.ReportObjects.Count > 0)
            {
                int index, count, counter;
                SubreportObject mySubReportObject;
                ReportDocument mySubRepDoc;

                for (index = 0; index <= rptDoc.ReportDefinition.Sections.Count - 1; index++)
                {
                    for (count = 0; count <= rptDoc.ReportDefinition.Sections[index].ReportObjects.Count - 1; count++)
                    {
                        Section rptSection = rptDoc.ReportDefinition.Sections[index];
                        if (rptSection.ReportObjects[count].Kind == ReportObjectKind.SubreportObject)
                        {
                            mySubReportObject = (SubreportObject)rptSection.ReportObjects[count];
                            mySubRepDoc = mySubReportObject.OpenSubreport(mySubReportObject.SubreportName);
                            for (counter = 0; counter <= mySubRepDoc.Database.Tables.Count - 1; counter++)
                            {
                                mySubRepDoc.Database.Tables[counter].ApplyLogOnInfo(conInfo);
                                if (!mySubRepDoc.Database.Tables[counter].TestConnectivity())
                                {
                                    testReturn = false;
                                    break;
                                }

                            }

                        }
                    }

                }

            }

            return testReturn;

        }
    }
}