﻿using CHRIS_REPORTING.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace CHRIS_REPORTING.Services
{
    public class CmnDataManager : CommonDataManager
    {

        private const string m_strActionType = "ActionType";
        private const string m_idColumn = "ID";

        public CmnDataManager() 
        {
            ConnectionBean bean = new ConnectionBean();
            SQLManager.SetConnectionBean(bean);
        }
        public virtual void PopulateDataTableFromObject(DataTable dtl, string pstrActionType
                                        , string pstrSearchText, string pstrSearchColumn, int pstrId)
        {
            DataRow drw = null;
            drw = dtl.NewRow();


            if (pstrActionType != string.Empty)
            {
                if (dtl.Columns.Contains(m_strActionType))
                    drw[m_strActionType] = pstrActionType;
            }

            if (pstrSearchText != string.Empty)
            {
                if (dtl.Columns.Contains(pstrSearchColumn))
                    drw[pstrSearchColumn] = pstrSearchText;
            }

            //if (pstrId.ToString() != string.Empty)
            //{
            //    if (dtl.Columns.Contains(pstrSearchColumn))
            //        drw[m_idColumn] = pstrId;
            //}
            dtl.Rows.Add(drw);
            dtl.AcceptChanges();
        }

        public virtual void PopulateDataTableFromObject(DataTable dtl, string pstrActionType
                                       , string[] paramValues, string[] paramColumns)
        {
            DataRow drw = null;
            drw = dtl.NewRow();


            if (pstrActionType != string.Empty)
            {
                if (dtl.Columns.Contains(m_strActionType))
                    drw[m_strActionType] = pstrActionType;
            }

            //if (pstrSearchText != string.Empty)
            //{
            //    if (dtl.Columns.Contains(pstrSearchColumn))
            //        drw[pstrSearchColumn] = pstrSearchText;
            //}

            for (int i = 0; (paramColumns.Length == paramValues.Length && i < paramColumns.Length); i++)
            {
                string paramCol = paramColumns[i];
                string paramVal = paramValues[i];
                if (paramVal != string.Empty && paramVal != null)
                {
                    if (dtl.Columns.Contains(paramCol))
                        drw[paramCol] = paramVal;
                }
            }

            //if (pstrId.ToString() != string.Empty)
            //{
            //    if (dtl.Columns.Contains(pstrSearchColumn))
            //        drw[m_idColumn] = pstrId;
            //}

            //drw[m_idColumn] = -1;

            dtl.Rows.Add(drw);
            dtl.AcceptChanges();
        }

        public virtual void PopulateDataTableFromObject(DataTable dtl, string pstrActionType
                                     , Dictionary<string, object> colsNValues)
        {
            DataRow drw = null;
            drw = dtl.NewRow();


            if (pstrActionType != string.Empty)
            {
                if (dtl.Columns.Contains(m_strActionType))
                    drw[m_strActionType] = pstrActionType;
            }

            //if (pstrSearchText != string.Empty)
            //{
            //    if (dtl.Columns.Contains(pstrSearchColumn))
            //        drw[pstrSearchColumn] = pstrSearchText;
            //}

            System.Collections.IEnumerator en = colsNValues.Keys.GetEnumerator();

            while (en.MoveNext())
            {
                string key = en.Current.ToString();
                object val = colsNValues[key];

                if (val != null && val.ToString() != String.Empty)
                {
                    if (dtl.Columns.Contains(key))
                        drw[key] = val;
                }

            }
            //for (int i = 0; i < colsNValues.Keys.Length; i++)
            //{
            //    string colName = colsNValues.Keys.
            //    string paramCol = paramColumns[i];
            //    string paramVal = paramValues[i];
            //    if (paramVal != string.Empty && paramVal != null)
            //    {
            //        if (dtl.Columns.Contains(paramCol))
            //            drw[paramCol] = paramVal;
            //    }
            //}

            //if (pstrId.ToString() != string.Empty)
            //{
            //    if (dtl.Columns.Contains(pstrSearchColumn))
            //        drw[m_idColumn] = pstrId;
            //}

            //drw[m_idColumn] = -1;

            dtl.Rows.Add(drw);
            dtl.AcceptChanges();
        }
        public Result GetData(string pstrSPName, string pstrActionType)
        {
            DataSet dst = new DataSet();

            this.spName = pstrSPName;
            dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            //PortfolioGroupSetupCommand pgSetup = new PortfolioGroupSetupCommand();

            this.PopulateDataTableFromObject(dst.Tables[this.spName], pstrActionType, "", "");

            return SQLManager.SelectDataSet(dst);
        }
        public Result GetData(string pstrSPName, string pstrActionType, string pstrSearchText
                                , string pstrSearchColumn, int SearchId)
        {
            DataSet dst = new DataSet();

            this.spName = pstrSPName;
            dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            this.PopulateDataTableFromObject(dst.Tables[this.spName], pstrActionType, pstrSearchText, pstrSearchColumn, SearchId);

            return SQLManager.SelectDataSet(dst);
        }

        public Result GetData(string pstrSPName, string pstrActionType, string[] paramValues
                               , string[] paramColumns)
        {
            DataSet dst = new DataSet();

            this.spName = pstrSPName;
            dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            this.PopulateDataTableFromObject(dst.Tables[this.spName], pstrActionType, paramValues, paramColumns);

            return SQLManager.SelectDataSet(dst);
        }

        public Result Execute(string pstrSPName, string pstrActionType, Dictionary<string, object> colsNVals)
        {
            DataSet dst = new DataSet();

            this.spName = pstrSPName;
            dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            this.PopulateDataTableFromObject(dst.Tables[this.spName], pstrActionType, colsNVals);

            return SQLManager.ExecuteDML(dst);
        }
        //added for payroll
        public Result Execute(string pstrSPName, string pstrActionType, Dictionary<string, object> colsNVals, ref SqlTransaction trans)
        {
            DataSet dst = new DataSet();

            this.spName = pstrSPName;
            dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            this.PopulateDataTableFromObject(dst.Tables[this.spName], pstrActionType, colsNVals);

            return SQLManager.ExecuteDML(dst, ref trans);
        }

        public Result Execute(string pstrSPName, string pstrActionType, Dictionary<string, object> colsNVals, ref SqlTransaction trans, int? timeout)
        {
            DataSet dst = new DataSet();

            this.spName = pstrSPName;
            dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            this.PopulateDataTableFromObject(dst.Tables[this.spName], pstrActionType, colsNVals);

            return SQLManager.ExecuteDML(dst, ref trans, timeout);
        }

        public Result Execute(string pstrSPName, string pstrActionType, Dictionary<string, object> colsNVals, int? timeOut)
        {
            DataSet dst = new DataSet();

            this.spName = pstrSPName;
            dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            this.PopulateDataTableFromObject(dst.Tables[this.spName], pstrActionType, colsNVals);

            return SQLManager.SelectDataSet(dst, timeOut);
        }

        public Result GetData(string pstrSPName, string pstrActionType, Dictionary<string, object> colsNVals)
        {
            DataSet dst = new DataSet();

            this.spName = pstrSPName;
            dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            this.PopulateDataTableFromObject(dst.Tables[this.spName], pstrActionType, colsNVals);

            return SQLManager.SelectDataSet(dst);
        }

        public Result Get(string SPName, string ActionType)
        {
            Result rslt;
            //IDataManager entityDataManager;
            //entityDataManager = RuntimeClassLoader.GetDataManager("");
            rslt = GetData(SPName, ActionType);
            return rslt;
        }
        public Result Get(string SPName, string ActionType, string SearchText
                            , string SearchColumn, int SearchId)
        {
            Result rslt;
            //IDataManager entityDataManager;
            //entityDataManager = RuntimeClassLoader.GetDataManager("");
            rslt = GetData(SPName, ActionType, SearchText, SearchColumn, SearchId);
            return rslt;
        }
        /// <summary>
        /// Muhammad Zia Ullah Baig
        /// (COM-788) CHRIS - Housing loan markup update to 365 days
        /// </summary>
        public bool SetMarkUp_Rate(string SPName, ref int MARKUP_RATE_DAYS)
        {
            string sMarkUp_Rate = string.Empty;
            Dictionary<string, object> param = new Dictionary<string, object>();
            Result dsList = GetData(SPName, "GET_MARKUP_RATE", param);
            bool isMarkUp_Valid = false;
            if (dsList.dstResult != null)
            {
                if (dsList.dstResult.Tables[0].Rows.Count > 0)
                {
                    sMarkUp_Rate = dsList.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    if (!string.IsNullOrEmpty(sMarkUp_Rate))
                    {
                        Int32.TryParse(sMarkUp_Rate, out MARKUP_RATE_DAYS);
                    }
                }
            }
            if (dsList.dstResult == null || dsList.dstResult.Tables[0].Rows.Count == 0 || MARKUP_RATE_DAYS <= 0)
            {
                isMarkUp_Valid = false;
            }
            else
                isMarkUp_Valid = true;
            return isMarkUp_Valid;
        }
    }
}
