﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace CHRIS_REPORTING.Services
{
    public class ConnectionBean
    {

        String constr = null;
        private string m_UserId;

        public string UserId
        {
            get { return m_UserId; }
        }
        private string m_Password;

        public string Pwd
        {
            get { return m_Password; }
        }

        private string M_DbName;

        public string DbName
        {
            get { return M_DbName; }
        }
        private string M_Server;

        public string M_ServerName
        {
            get { return M_Server; }

        }

        // over loaded method for connection bean 
        // this constructor is used when using xms authentication
        public ConnectionBean()
        {
            //IConfigurationRoot configuration = new ConfigurationBuilder()
            //    .SetBasePath(Directory.GetCurrentDirectory())
            //    .AddJsonFile("appsettings.json")
            //    .Build();
            constr = System.Configuration.ConfigurationManager.ConnectionStrings["CHRIS_SERVICE_Entities"].ConnectionString;
            if (constr != null)
            {
                int dbname_start_index = constr.IndexOf("Catalog=") + 8;
                int dbname_end_index = constr.IndexOf(";Integrated");
                int dbname_length = dbname_end_index - dbname_start_index;
                M_DbName = constr.Substring(dbname_start_index, dbname_length );

                int server_name_start_index = constr.IndexOf("Data Source=") + 12;
                int server_name_end_index = constr.IndexOf(";Initial Catalog");
                int server_name_length = server_name_end_index - server_name_start_index;
                M_Server = constr.Substring(server_name_start_index, server_name_length);

            }
            /*configuration.GetConnectionString("cs");*/
        }

        public SqlConnection getDatabaseConnection()
        {
            SqlConnection sqlconn = new SqlConnection();
            sqlconn.ConnectionString = constr;
            sqlconn.Open();
            return sqlconn;
        }
        public SqlConnection getDatabaseConnection(int timeout)
        {
            SqlConnection sqlconn = new SqlConnection();
            sqlconn.ConnectionString = constr + ";Connect Timeout=" + timeout + ";";
            sqlconn.Open();
            return sqlconn;
        }


        public int doInsertUpdate(String str_sql, SqlConnection sqlconn)
        {
            int i = 0;
            try
            {
                SqlCommand insert_command = new SqlCommand(str_sql, sqlconn);
                i = insert_command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                StreamWriter sw = getLogfile();
                sw.WriteLine("------------------------------------------------------------------");
                sw.WriteLine("Time of logging:" + DateTime.Now);
                sw.WriteLine("Exception come in Connection bean method doInsertUpdate");
                sw.WriteLine(ex.Message);
                sw.WriteLine("------------------------------------------------------------------");
                sw.Close();

            }
            return i;
        }

        public SqlDataReader getData(String str_sql, SqlConnection sqlconn)
        {
            SqlCommand selectcommand = sqlconn.CreateCommand();
            selectcommand.CommandText = str_sql;
            SqlDataReader selectreader = selectcommand.ExecuteReader();
            return selectreader;
        }
        public DataTable getDataTable(String str_sql, SqlConnection sqlconn)
        {
            SqlCommand selectedcommand = new SqlCommand();
            selectedcommand = sqlconn.CreateCommand();
            selectedcommand.CommandText = str_sql;
            selectedcommand.CommandType = CommandType.Text;
            DataTable dt = new DataTable("DataTable");
            SqlDataAdapter da = new SqlDataAdapter(selectedcommand);
            da.Fill(dt);
            return dt;
        }
        public void closeReader(SqlDataReader sqlreader)
        {

            sqlreader.Close();
            sqlreader.Dispose();
        }

        public void closeConn(SqlConnection sqlconn)
        {
            sqlconn.Close();
            sqlconn.Dispose();
        }
        public StreamWriter getLogfile()
        {
            string complete_path = "C:/iCORE-Logs/FXRR-Logs/";
            string filename = DateTime.Now.Date.Day + "_" + DateTime.Now.Date.Month + "_" + DateTime.Now.Date.Year + ".txt";
            string path = complete_path + filename;
            StreamWriter sw = File.AppendText(path);
            return sw;
        }
    }
}
