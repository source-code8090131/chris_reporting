﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using CHRIS_REPORTING.Services;
using System.Diagnostics;

namespace CHRIS_REPORTING.Common
{
    public class SQLManager
    {
        private static DataSet dstParams = new DataSet();
        private static  ConnectionBean connbean = null;
        internal static string userId = null;
        private const string ActionType = "ActionType";

        /// <summary>
        /// Set Connection object
        /// </summary>
        /// <param name="connbean_obj"></param>
        /// 


        public static void SetConnectionBean(ConnectionBean connbean_obj)
        {
            if (connbean == null)
                connbean = connbean_obj;
        }

        /// <summary>
        /// Return Connection Object
        /// </summary>
        /// <returns></returns>
        public static ConnectionBean GetConnectionBean()
        {
            return connbean;
        }

        /// <summary>
        /// Set USer ID
        /// </summary>
        /// <param name="usrId"></param>
        public static void SetFormInfo(string usrId)
        {
            if (userId == null)
                userId = usrId;
        }

        /// <summary>
        /// Execute Query
        /// </summary>
        /// <param name="dstParams"></param>
        /// <returns></returns>
        public static Result ExecuteDML(DataSet dstParams)
        {
            Result rslt = new Result();
            SqlCommand cmd = null;
            SqlTransaction trans = null;
            SqlConnection conn = null;
            rslt.isSuccessful = false;
            rslt.hstOutParams = new Hashtable();

            try
            {
                conn = connbean.getDatabaseConnection();
                trans = conn.BeginTransaction();

                foreach (DataTable dtlParams in dstParams.Tables)
                {
                    cmd = new SqlCommand(dtlParams.TableName, conn, trans);
                    cmd.CommandType = CommandType.StoredProcedure;

                    for (int i = 0; i < dtlParams.Rows.Count; i++)
                    {
                        CreateCommandParameters(ref cmd, dtlParams, i);

                        cmd.ExecuteNonQuery();

                        foreach (SqlParameter param in cmd.Parameters)
                        {
                            if (param.Direction == ParameterDirection.InputOutput ||
                                param.Direction == ParameterDirection.Output ||
                                param.Direction == ParameterDirection.ReturnValue)
                            {
                                dtlParams.Rows[i][param.ParameterName] =
                                    param.Value;
                                //rslt.hstOutParams.Add(GetTableName(dtlParams.TableName) + "." +
                                //    param.ParameterName.Substring(0) + i, param.Value);
                                rslt.hstOutParams.Add("Table0." + param.ParameterName.Substring(0) + i, param.Value);
                            }
                        }
                    }
                }

                trans.Commit();
                rslt.isSuccessful = true;

                // Comment by SL, need to enable later.
                //LogDMLAcitivities(dstParams);
            }
            catch (Exception ex)
            {
                trans.Rollback();

                rslt.isSuccessful = false;
                rslt.exp = ex;
                rslt.message = ex.Message.StartsWith("DELETE statement conflicted with COLUMN REFERENCE constraint") ?
                    "Reference records found in other tables." : ex.Message;

                LogException("SQLManager", "ExecuteDML", ex);
            }
            finally
            {
                connbean.closeConn(conn);

                conn = null;
                cmd = null;
                trans = null;
            }

            return rslt;
        }


        /// <summary>
        /// Execute Query with transactions
        /// </summary>
        /// <param name="dstParams"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public static Result ExecuteDML(DataSet dstParams, ref SqlTransaction trans)
        {
            SqlDataAdapter adp = null;
            DataSet dst = null;
            Result rslt = new Result();
            SqlCommand cmd = null;
            //SqlTransaction trans = null;
            SqlConnection conn = null;
            rslt.isSuccessful = false;
            rslt.hstOutParams = new Hashtable();
            rslt.dstResult = new DataSet();
            try
            {

                if (trans == null)
                {
                    conn = connbean.getDatabaseConnection();
                    trans = conn.BeginTransaction();
                }
                else
                {
                    conn = trans.Connection;
                }

                foreach (DataTable dtlParams in dstParams.Tables)
                {
                    cmd = new SqlCommand(dtlParams.TableName, conn, trans);
                    cmd.CommandType = CommandType.StoredProcedure;

                    for (int i = 0; i < dtlParams.Rows.Count; i++)
                    {
                        CreateCommandParameters(ref cmd, dtlParams, i);

                        // cmd.ExecuteNonQuery();
                        adp = new SqlDataAdapter(cmd);
                        dst = new DataSet();
                        adp.Fill(dst, dtlParams.TableName);
                        foreach (DataTable dtl in dst.Tables)
                        {
                            rslt.dstResult.Tables.Add(dtl.Copy());
                        }
                        foreach (SqlParameter param in cmd.Parameters)
                        {
                            if (param.Direction == ParameterDirection.InputOutput ||
                                param.Direction == ParameterDirection.Output ||
                                param.Direction == ParameterDirection.ReturnValue)
                            {
                                dtlParams.Rows[i][param.ParameterName] =
                                    param.Value;
                                //rslt.hstOutParams.Add(GetTableName(dtlParams.TableName) + "." +
                                //    param.ParameterName.Substring(0) + i, param.Value);
                                rslt.hstOutParams.Add("Table0." + param.ParameterName.Substring(0) + i, param.Value);
                            }
                        }
                    }
                }

                //trans.Commit();
                rslt.isSuccessful = true;

                // Comment by SL, need to enable later.
                //LogDMLAcitivities(dstParams);
            }
            catch (Exception ex)
            {
                //trans.Rollback();

                rslt.isSuccessful = false;
                rslt.exp = ex;
                rslt.message = ex.Message.StartsWith("DELETE statement conflicted with COLUMN REFERENCE constraint") ?
                    "Reference records found in other tables." : ex.Message;

                LogException("SQLManager", "ExecuteDML", ex);
            }
            finally
            {
                //connbean.closeConn(conn);

                conn = null;
                cmd = null;
                //trans = null;
            }

            return rslt;
        }


        /// <summary>
        /// Execute Query with transactions
        /// </summary>
        /// <param name="dstParams"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public static Result ExecuteDML(DataSet dstParams, ref SqlTransaction trans, int? timeout)
        {
            SqlDataAdapter adp = null;
            DataSet dst = null;
            Result rslt = new Result();
            SqlCommand cmd = null;
            //SqlTransaction trans = null;
            SqlConnection conn = null;
            rslt.isSuccessful = false;
            rslt.hstOutParams = new Hashtable();
            rslt.dstResult = new DataSet();
            try
            {

                if (timeout != null)
                    conn = connbean.getDatabaseConnection((int)timeout);
                else
                    conn = connbean.getDatabaseConnection();

                if (trans == null)
                {
                    trans = conn.BeginTransaction();
                }
                else
                {
                    conn = trans.Connection;
                }

                foreach (DataTable dtlParams in dstParams.Tables)
                {
                    cmd = new SqlCommand(dtlParams.TableName, conn, trans);
                    cmd.CommandType = CommandType.StoredProcedure;

                    for (int i = 0; i < dtlParams.Rows.Count; i++)
                    {
                        CreateCommandParameters(ref cmd, dtlParams, i);

                        // cmd.ExecuteNonQuery();
                        adp = new SqlDataAdapter(cmd);
                        dst = new DataSet();
                        adp.Fill(dst, dtlParams.TableName);
                        foreach (DataTable dtl in dst.Tables)
                        {
                            rslt.dstResult.Tables.Add(dtl.Copy());
                        }
                        foreach (SqlParameter param in cmd.Parameters)
                        {
                            if (param.Direction == ParameterDirection.InputOutput ||
                                param.Direction == ParameterDirection.Output ||
                                param.Direction == ParameterDirection.ReturnValue)
                            {
                                dtlParams.Rows[i][param.ParameterName] =
                                    param.Value;
                                //rslt.hstOutParams.Add(GetTableName(dtlParams.TableName) + "." +
                                //    param.ParameterName.Substring(0) + i, param.Value);
                                rslt.hstOutParams.Add("Table0." + param.ParameterName.Substring(0) + i, param.Value);
                            }
                        }
                    }
                }

                //trans.Commit();
                rslt.isSuccessful = true;

                // Comment by SL, need to enable later.
                //LogDMLAcitivities(dstParams);
            }
            catch (Exception ex)
            {
                //trans.Rollback();

                rslt.isSuccessful = false;
                rslt.exp = ex;
                rslt.message = ex.Message.StartsWith("DELETE statement conflicted with COLUMN REFERENCE constraint") ?
                    "Reference records found in other tables." : ex.Message;

                LogException("SQLManager", "ExecuteDML", ex);
            }
            finally
            {
                //connbean.closeConn(conn);

                conn = null;
                cmd = null;
                //trans = null;
            }

            return rslt;
        }


        /// <summary>
        /// Obsoleted Method. Logging DML
        /// </summary>
        /// <param name="dstParams"></param>
        private static void LogDMLAcitivities(DataSet dstParams)
        {
            SqlCommand cmd = null;
            SqlTransaction trans = null;
            SqlConnection conn = null;

            try
            {
                conn = connbean.getDatabaseConnection();
                trans = conn.BeginTransaction();

                foreach (DataTable dtlParams in dstParams.Tables)
                {
                    cmd = new SqlCommand("SL_SP_DMLActivityLog_Add", conn, trans);
                    cmd.CommandType = CommandType.StoredProcedure;

                    for (int i = 0; i < dtlParams.Rows.Count; i++)
                    {
                        cmd.Parameters.Clear();

                        cmd.Parameters.Add("@TableName", SqlDbType.VarChar);
                        cmd.Parameters["@TableName"].Value = GetTableName(dtlParams.TableName);
                        cmd.Parameters.Add("@SPName", SqlDbType.VarChar);
                        cmd.Parameters["@SPName"].Value = dtlParams.TableName;
                        cmd.Parameters.Add("@RecordData", SqlDbType.VarChar);
                        cmd.Parameters["@RecordData"].Value = GetData(dtlParams.Rows[i]);
                        cmd.Parameters.Add("@UserID", SqlDbType.VarChar);
                        cmd.Parameters["@UserID"].Value = userId;
                        cmd.Parameters.Add("@Activity", SqlDbType.VarChar);
                        cmd.Parameters["@Activity"].Value =
                            dtlParams.TableName.Substring(dtlParams.TableName.LastIndexOf("_") + 1);

                        cmd.ExecuteNonQuery();
                    }
                }

                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();

                LogException("SQLManager", "LogDMLAcitivities", ex);
            }
            finally
            {
                connbean.closeConn(conn);

                conn = null;
                cmd = null;
                trans = null;
            }
        }

        /// <summary>
        /// Log Exceptions
        /// </summary>
        /// <param name="className"></param>
        /// <param name="functionName"></param>
        /// <param name="exp"></param>
        public static void LogException(string className, string functionName, Exception exp)
        {
            // Introduce Custom Loging 
            Debug.Print("introduce logging in SqlManager");
        }

        /// <summary>
        /// GetTableName
        /// </summary>
        /// <param name="spName"></param>
        /// <returns></returns>
        protected internal static string GetTableName(string spName)
        {
            string tableName = "";

            tableName = "SL_" + spName.Substring(7, spName.IndexOf("_", 7) - 7);

            return tableName;
        }

        /// <summary>
        /// GetData
        /// </summary>
        /// <param name="drw"></param>
        /// <returns></returns>
        private static string GetData(DataRow drw)
        {
            string data = "";

            if (drw.Table.Columns.Contains("Id"))
            {
                data = "Id=" + drw["Id"];
            }
            else
            {
                foreach (DataColumn dcl in drw.Table.Columns)
                {
                    data += dcl.ColumnName + "=" + drw[dcl.ColumnName] + ",";
                }
            }

            return data;
        }

        /// <summary>
        /// Get ResultSet
        /// </summary>
        /// <param name="dstParams"></param>
        /// <returns></returns>
        public static Result SelectDataSet(DataSet dstParams)
        {
            return SelectDataSet(dstParams, null);
        }

        /// <summary>
        /// Get ResultSet
        /// </summary>
        /// <param name="dstParams"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public static Result SelectDataSet(DataSet dstParams, int? timeout)
        {
            Result rslt = new Result();
            SqlCommand cmd = null;
            SqlDataAdapter adp = null;
            SqlConnection conn = null;
            rslt.isSuccessful = false;
            DataSet dst = null;
            rslt.dstResult = new DataSet();

            try
            {
                if (timeout != null)
                    conn = connbean.getDatabaseConnection((int)timeout);
                else
                    conn = connbean.getDatabaseConnection();

                foreach (DataTable dtlParams in dstParams.Tables)
                {
                    cmd = new SqlCommand(dtlParams.TableName, conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    if (timeout != null && cmd != null)
                        cmd.CommandTimeout = (int)timeout;


                    CreateCommandParameters(ref cmd, dtlParams);

                    adp = new SqlDataAdapter(cmd);
                    dst = new DataSet();

                    adp.Fill(dst, dtlParams.TableName);

                    foreach (DataTable dtl in dst.Tables)
                    {
                        rslt.dstResult.Tables.Add(dtl.Copy());
                    }
                }

                rslt.isSuccessful = true;
            }
            catch (Exception ex)
            {
                rslt.isSuccessful = false;
                rslt.exp = ex;
                rslt.message = ex.Message;

                LogException("SQLManager", "SelectDataSet", ex);
            }
            finally
            {
                connbean.closeConn(conn);

                conn = null;
                cmd = null;
                adp = null;
                dst = null;
            }

            return rslt;
        }

        /// <summary>
        /// Get ResultSet with transactions
        /// </summary>
        /// <param name="dstParams"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public static Result SelectDataSet(DataSet dstParams, ref SqlTransaction trans)
        {
            Result rslt = new Result();
            SqlCommand cmd = null;
            SqlDataAdapter adp = null;
            SqlConnection conn = null;
            rslt.isSuccessful = false;
            DataSet dst = null;
            rslt.dstResult = new DataSet();

            try
            {
                if (trans == null)
                {
                    conn = connbean.getDatabaseConnection();
                    trans = conn.BeginTransaction();
                }
                else
                {
                    conn = trans.Connection;
                }


                foreach (DataTable dtlParams in dstParams.Tables)
                {
                    cmd = new SqlCommand(dtlParams.TableName, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Transaction = trans;
                    CreateCommandParameters(ref cmd, dtlParams);

                    adp = new SqlDataAdapter(cmd);
                    dst = new DataSet();

                    adp.Fill(dst, dtlParams.TableName);

                    foreach (DataTable dtl in dst.Tables)
                    {
                        rslt.dstResult.Tables.Add(dtl.Copy());
                    }
                }

                rslt.isSuccessful = true;
            }
            catch (Exception ex)
            {

                rslt.isSuccessful = false;
                rslt.exp = ex;
                rslt.message = ex.Message;

                LogException("SQLManager", "SelectDataSet", ex);
            }
            finally
            {
                //connbean.closeConn(conn);

                // conn = null;
                cmd = null;
                adp = null;
                dst = null;
            }

            return rslt;
        }

        /// <summary>
        /// Get ResultSet
        /// </summary>
        /// <param name="spName"></param>
        /// <returns></returns>
        public static Result SelectDataSet(string spName)
        {
            Result rslt = new Result();
            SqlCommand cmd = null;
            SqlDataAdapter adp = null;
            SqlConnection conn = null;
            rslt.isSuccessful = false;
            DataSet dst = null;
            rslt.dstResult = new DataSet();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand(spName, conn);
                cmd.CommandType = CommandType.StoredProcedure;

                adp = new SqlDataAdapter(cmd);
                dst = new DataSet();

                adp.Fill(dst);

                foreach (DataTable dtl in dst.Tables)
                {
                    rslt.dstResult.Tables.Add(dtl.Copy());
                }

                rslt.isSuccessful = true;
            }
            catch (Exception ex)
            {

                rslt.isSuccessful = false;
                rslt.exp = ex;
                rslt.message = ex.Message;

                LogException("SQLManager", "SelectDataSet(string spName)", ex);
            }
            finally
            {
                connbean.closeConn(conn);

                conn = null;
                cmd = null;
                adp = null;
                dst = null;
            }

            return rslt;
        }

        /// <summary>
        /// Excecute Scalar
        /// </summary>
        /// <param name="dtlParams"></param>
        /// <returns></returns>
        public static Result SelectScaler(DataTable dtlParams)
        {
            Result rslt = new Result();
            SqlCommand cmd = null;
            SqlConnection conn = null;
            rslt.isSuccessful = false;

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand(dtlParams.TableName, conn);
                cmd.CommandType = CommandType.StoredProcedure;

                CreateCommandParameters(ref cmd, dtlParams);

                rslt.objResult = cmd.ExecuteScalar();
                rslt.isSuccessful = true;
            }
            catch (Exception ex)
            {
                rslt.isSuccessful = false;
                rslt.exp = ex;
                rslt.message = ex.Message;

                LogException("SQLManager", "SelectScaler", ex);
            }
            finally
            {
                connbean.closeConn(conn);

                conn = null;
                cmd = null;
            }

            return rslt;
        }

        /// <summary>
        /// Excecute Scalar
        /// </summary>
        /// <param name="spName"></param>
        /// <returns></returns>
        public static Result SelectScaler(string spName)
        {
            Result rslt = new Result();
            SqlCommand cmd = null;
            SqlConnection conn = null;
            rslt.isSuccessful = false;

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand(spName, conn);
                cmd.CommandType = CommandType.StoredProcedure;

                rslt.objResult = cmd.ExecuteScalar();
                rslt.isSuccessful = true;
            }
            catch (Exception ex)
            {
                rslt.isSuccessful = false;
                rslt.exp = ex;
                rslt.message = ex.Message;

                LogException("SQLManager", "SelectScaler(string spName)", ex);
            }
            finally
            {
                connbean.closeConn(conn);

                conn = null;
                cmd = null;
            }

            return rslt;
        }

        /// <summary>
        /// Create Parameters
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="dtlParams"></param>
        private static void CreateCommandParameters(ref SqlCommand cmd, DataTable dtlParams)
        {
            CreateCommandParameters(ref cmd, dtlParams, 0);
        }

        /// <summary>
        /// Create Parameters
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="dtlParams"></param>
        /// <param name="rowIndex"></param>
        private static void CreateCommandParameters(ref SqlCommand cmd, DataTable dtlParams, int rowIndex)
        {
            int i = 0;
            SqlParameter parameter = null;

            try
            {
                cmd.Parameters.Clear();

                foreach (DataColumn dcl in dtlParams.Columns)
                {
                    parameter = new SqlParameter(dcl.ColumnName, GetDBType(dcl.DataType.ToString()));
                    parameter.Value = dtlParams.Rows[rowIndex][i];

                    if (Convert.ToBoolean(dcl.ExtendedProperties["IsOutParam"]))
                        parameter.Direction = ParameterDirection.InputOutput;
                    else
                        parameter.Direction = ParameterDirection.Input;

                    if (dcl.ExtendedProperties["length"] != DBNull.Value)
                        parameter.Size = Convert.ToInt32(dcl.ExtendedProperties["length"]);
                    if (dcl.ExtendedProperties["scale"] != DBNull.Value)
                        parameter.Scale = Convert.ToByte(dcl.ExtendedProperties["scale"]);
                    if (dcl.ExtendedProperties["prec"] != DBNull.Value)
                        parameter.Precision = Convert.ToByte(dcl.ExtendedProperties["prec"]);

                    cmd.Parameters.Add(parameter);
                    i++;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "CreateCommandParameters", ex);
            }
        }

        /// <summary>
        /// SP Param Dynamic Discovery 
        /// </summary>
        /// <param name="strSPName"></param>
        /// <returns></returns>
        public static DataTable GetSPParams(string strSPName)
        {
            if (!dstParams.Tables.Contains(strSPName))
                CreateSPParams(strSPName);

            return dstParams.Tables[strSPName].Copy();
        }

        /// <summary>
        /// SP Param Data Table with extended properties
        /// </summary>
        /// <param name="strSPName"></param>
        private static void CreateSPParams(string strSPName)
        {
            DataSet dst = new DataSet();
            DataTable dtlParam = new DataTable(strSPName);
            SqlCommand cmd = null;
            SqlDataAdapter adp = null;
            SqlConnection conn = null;

            try
            {
                try
                {
                    conn = connbean.getDatabaseConnection();
                    cmd = new SqlCommand("CMN_SP_StoredProcedures_GetParams", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@SPName", SqlDbType.VarChar);
                    cmd.Parameters["@SPName"].Value = strSPName;

                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(dst);
                }
                catch (Exception ex)
                {
                    LogException("SQLManager", "Inner Block CreateSPParams", ex);

                    throw ex;
                }
                finally
                {
                    connbean.closeConn(conn);

                    conn = null;
                    cmd = null;
                    adp = null;
                }

                foreach (DataRow drw in dst.Tables[0].Rows)
                {
                    dtlParam.Columns.Add(drw["ParamName"].ToString(), GetSystemType(drw["ParamType"].ToString()));
                    dtlParam.Columns[drw["ParamName"].ToString()].ExtendedProperties.Add("IsOutParam",
                        drw["IsOutParam"]);
                    dtlParam.Columns[drw["ParamName"].ToString()].ExtendedProperties.Add("prec",
                        drw["prec"]);
                    dtlParam.Columns[drw["ParamName"].ToString()].ExtendedProperties.Add("scale",
                        drw["scale"]);
                    dtlParam.Columns[drw["ParamName"].ToString()].ExtendedProperties.Add("length",
                        drw["length"]);
                }

                dtlParam.AcceptChanges();
                dstParams.Tables.Add(dtlParam);
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "CreateSPParams", ex);

                throw ex;
            }
        }

        /// <summary>
        /// Get DB Type
        /// </summary>
        /// <param name="strSystemType"></param>
        /// <returns></returns>
        private static SqlDbType GetDBType(string strSystemType)
        {
            SqlDbType dbType = SqlDbType.VarChar;

            switch (strSystemType)
            {
                case "System.String":
                    dbType = SqlDbType.VarChar;
                    break;
                case "System.Int64":
                    dbType = SqlDbType.BigInt;
                    break;
                case "System.Int32":
                    dbType = SqlDbType.Int;
                    break;
                case "System.DateTime":
                    dbType = SqlDbType.DateTime;
                    break;
                case "System.Double":
                    dbType = SqlDbType.Float;
                    break;
                case "System.Boolean":
                    dbType = SqlDbType.Bit;
                    break;
                case "System.Byte[]":
                    dbType = SqlDbType.Binary;
                    break;
                case "System.Single":
                    dbType = SqlDbType.Float;
                    break;
                case "System.Int16":
                    dbType = SqlDbType.SmallInt;
                    break;
                case "System.Decimal":
                    dbType = SqlDbType.Decimal;
                    break;
            }

            return dbType;
        }

        /// <summary>
        /// Get System Type
        /// </summary>
        /// <param name="strSQLType"></param>
        /// <returns></returns>
        private static Type GetSystemType(string strSQLType)
        {
            Type type = Type.GetType("System.String");

            switch (strSQLType)
            {
                case "text":
                    type = Type.GetType("System.String");
                    break;
                case "tinyint":
                    type = Type.GetType("System.Int32");
                    break;
                case "smallint":
                    type = Type.GetType("System.Int32");
                    break;
                case "int":
                    type = Type.GetType("System.Int32");
                    break;
                case "smalldatetime":
                    type = Type.GetType("System.DateTime");
                    break;
                case "real":
                    type = Type.GetType("System.Double");
                    break;
                case "money":
                    type = Type.GetType("System.Double");
                    break;
                case "datetime":
                    type = Type.GetType("System.DateTime");
                    break;
                case "float":
                    type = Type.GetType("System.Double");
                    break;
                case "ntext":
                    type = Type.GetType("System.String");
                    break;
                case "bit":
                    type = Type.GetType("System.Boolean");
                    break;
                case "decimal":
                    type = Type.GetType("System.Double");
                    break;
                case "numeric":
                    //type = Type.GetType("System.Int32");
                    type = Type.GetType("System.Decimal");// UPDATED BY TARIQ AFZAL 
                    break;
                case "smallmoney":
                    type = Type.GetType("System.Double");
                    break;
                case "bigint":
                    type = Type.GetType("System.Int32");
                    break;
                case "varchar":
                    type = Type.GetType("System.String");
                    break;
                case "char":
                    type = Type.GetType("System.String");
                    break;
                case "nvarchar":
                    type = Type.GetType("System.String");
                    break;
                case "nchar":
                    type = Type.GetType("System.String");
                    break;
            }

            return type;
        }

        /// <summary>
        /// Check for Boolean Value
        /// </summary>
        /// <param name="boolValue"></param>
        /// <returns></returns>
        public static bool EvaluateBool(object boolValue)
        {
            if (boolValue is System.DBNull || boolValue == null)
                boolValue = false;

            return (bool)boolValue;
        }

        /// <summary>
        /// Check for String Value
        /// </summary>
        /// <param name="stringValue"></param>
        /// <returns></returns>
        public static string EvaluateString(object stringValue)
        {
            if (stringValue is System.DBNull || stringValue == null)
                stringValue = "";

            return stringValue.ToString();
        }

        #region Cost Center

        public static void ExecuteCostCenterDetailUpload(DataTable dt)
        {
            SqlConnection conn = null;
            try
            {
                conn = connbean.getDatabaseConnection();
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn, SqlBulkCopyOptions.KeepIdentity, null))
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                    }

                    bulkCopy.BulkCopyTimeout = 600;
                    bulkCopy.DestinationTableName = "[dbo].[TempDept_CostCode]";
                    bulkCopy.WriteToServer(dt);
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                //cmd = null;
            }
        }

        public static DataSet ExecutegetDept_CostCodeData(string UserID)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[SP_getDeptCostCodeTempData]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@UserID", SqlDbType.Text);
                cmd.Parameters["@UserID"].Value = UserID;
                cmd.CommandTimeout = 0;
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        public static DataSet exPFund(DateTime fromDate, DateTime toDate, string PR_Emp_No)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();
            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_SP_PFUND_Summary]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@fromDate", SqlDbType.Date);
                cmd.Parameters["@fromDate"].Value = fromDate;
                cmd.Parameters.Add("@toDate", SqlDbType.Date);
                cmd.Parameters["@toDate"].Value = toDate;
                cmd.Parameters.Add("@PR_Emp_No", SqlDbType.Text);
                cmd.Parameters["@PR_Emp_No"].Value = PR_Emp_No;

                cmd.CommandTimeout = 0;

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }


        public static DataSet ExecutegetDept()
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[SP_getAllDept]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        #region Maker Checker

        public static DataSet sp_MakerChecker(string sp_name, string frmCall, string userID)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand(sp_name, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@frmCall", SqlDbType.Text);
                cmd.Parameters["@frmCall"].Value = frmCall;
                cmd.Parameters.Add("@userID", SqlDbType.Text);
                cmd.Parameters["@userID"].Value = userID;
                cmd.CommandTimeout = 0;
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        //DeleteRowData
        public static DataSet sp_MC_DeleteRowData(string sp_name, int RowID)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand(sp_name, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@RowID", SqlDbType.Int);
                cmd.Parameters["@RowID"].Value = RowID;
                cmd.CommandTimeout = 0;
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        #endregion

        #region Update Dept Table Data 

        public static void ExecuteUpdateDept(string tableName, string Dept, string CostCenter)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                //string commandText = string.Format("Update {0} SET SP_COST_CENTER = '{2}' where SP_DEPT like '{1}'", tableName, Dept, CostCenter);

                cmd = new SqlCommand("[dbo].[SP_getDeptCostCodeUpdate]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                // cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.Add("@tableName", SqlDbType.Text);
                //cmd.Parameters["@tableName"].Value = tableName;
                cmd.Parameters.Add("@Dept", SqlDbType.Text);
                cmd.Parameters["@Dept"].Value = Dept;
                cmd.Parameters.Add("@CostCenter", SqlDbType.Text);
                cmd.Parameters["@CostCenter"].Value = CostCenter;

                cmd.CommandTimeout = 0;

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        #endregion

        public static DataSet ExecuteCHRIS_RPT_SP_PY007(string branch, string w_seg, int getmonth, int getyear, string cat)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            int month = Convert.ToInt32(getmonth);
            int year = Convert.ToInt32(getyear);
            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_RPT_SP_PY007]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@branch", SqlDbType.Text);
                cmd.Parameters["@branch"].Value = branch;
                cmd.Parameters.Add("@w_seg", SqlDbType.Text);
                cmd.Parameters["@w_seg"].Value = w_seg;
                cmd.Parameters.Add("@month", SqlDbType.Int);
                cmd.Parameters["@month"].Value = month;
                cmd.Parameters.Add("@year", SqlDbType.Int);
                cmd.Parameters["@year"].Value = year;
                cmd.Parameters.Add("@cat", SqlDbType.Text);
                cmd.Parameters["@cat"].Value = cat;
                cmd.CommandTimeout = 0;
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        #endregion


        #region Maker Checker Calling SP

        public static DataSet CHRIS_SP_PersonnelCheck(string userID)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_PERSONAL_CHECK_DETAIL]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@userID", SqlDbType.Text);
                cmd.Parameters["@userID"].Value = userID;
                cmd.CommandTimeout = 0;

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        public static DataSet CHRIS_SP_GET_PersonnelInfo(string PR_NO)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();
            int PRNO = Convert.ToInt32(PR_NO);
            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_GET_PERSONALINFO_PR_NO]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PRNO", SqlDbType.Int);
                cmd.Parameters["@PRNO"].Value = PRNO;
                cmd.CommandTimeout = 0;

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        public static DataSet CHRIS_SP_GET_Dept_Cont(string PR_NO)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();
            int PRNO = Convert.ToInt32(PR_NO);
            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_GET_DEPT_CONT_PR_NO]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PRNO", SqlDbType.Int);
                cmd.Parameters["@PRNO"].Value = PRNO;
                cmd.CommandTimeout = 0;

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        public static DataSet CHRIS_SP_GET_Education(string PR_NO)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();
            int PRNO = Convert.ToInt32(PR_NO);
            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_GET_EDUCATIONT_PR_NO]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PRNO", SqlDbType.Int);
                cmd.Parameters["@PRNO"].Value = PRNO;
                cmd.CommandTimeout = 0;

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        public static DataSet CHRIS_SP_GET_Experience(string PR_NO)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();
            int PRNO = Convert.ToInt32(PR_NO);
            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_GET_PRE_EMP_PR_NO]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PRNO", SqlDbType.Int);
                cmd.Parameters["@PRNO"].Value = PRNO;
                cmd.CommandTimeout = 0;

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        public static void ApprovedRegularHiring(int id, string UserID)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_APPROVED_PRE_EMP_PR_NO]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PRNO", SqlDbType.Int);
                cmd.Parameters["@PRNO"].Value = id;
                cmd.Parameters.Add("@UserID", SqlDbType.Text);
                cmd.Parameters["@UserID"].Value = UserID;
                cmd.CommandTimeout = 0;

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        public static void REJECTRegularHiring(int id, string UserID)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_REJECT_PRE_EMP_PR_NO]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PRNO", SqlDbType.Int);
                cmd.Parameters["@PRNO"].Value = id;
                cmd.Parameters.Add("@UserID", SqlDbType.Text);
                cmd.Parameters["@UserID"].Value = UserID;
                cmd.CommandTimeout = 0;

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }
        #endregion


        #region Confirmation Screen

        public static DataSet CHRIS_SP_ConfirmationCheck(string userID)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_Confirmation_DETAIL]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@userID", SqlDbType.Text);
                cmd.Parameters["@userID"].Value = userID;
                cmd.CommandTimeout = 0;

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }


        public static void ExecuteConDetailUpload(DataTable dt)
        {
            SqlConnection conn = null;
            try
            {
                conn = connbean.getDatabaseConnection();
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn, SqlBulkCopyOptions.KeepIdentity, null))
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                    }

                    bulkCopy.BulkCopyTimeout = 600;
                    bulkCopy.DestinationTableName = "[dbo].[PERSONNEL_CONFIRM]";
                    bulkCopy.WriteToServer(dt);
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                //cmd = null;
            }
        }

        public static DataSet CHRIS_SP_GET_Confir_PRNO(string PR_NO)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();
            int PRNO = Convert.ToInt32(PR_NO);
            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_Confirm_GET_PRNO]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PRNO", SqlDbType.Int);
                cmd.Parameters["@PRNO"].Value = PRNO;
                cmd.CommandTimeout = 0;

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        public static void ApprovedConfirmation(int id)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_Confirm_Update_PRNO]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PRNO", SqlDbType.Int);
                cmd.Parameters["@PRNO"].Value = id;
                //cmd.Parameters.Add("@UserID", SqlDbType.Text);
                //cmd.Parameters["@UserID"].Value = UserID;
                cmd.CommandTimeout = 0;

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        public static void ApprovedReject(int id)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_Confirm_Reject_PRNO]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PRNO", SqlDbType.Int);
                cmd.Parameters["@PRNO"].Value = id;
                //cmd.Parameters.Add("@UserID", SqlDbType.Text);
                //cmd.Parameters["@UserID"].Value = UserID;
                cmd.CommandTimeout = 0;

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }
        #endregion

        #region TERMINATION

        public static void PR_TERMINATION(int PR_P_NO, string PR_FIRST_NAME, string PR_TERMIN_TYPE, DateTime PR_TERMIN_DATE, string PR_REASONS, string PR_RESIG_RECV, string PR_APP_RESIG, string PR_EXIT_INTER, string PR_ID_RETURN, string PR_NOTICE,
                                              string PR_ARTONY, string PR_DELETION, string PR_SETTLE, string PR_UserID)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_PR_TERMINATION_CHK]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PR_P_NO", SqlDbType.Int);
                cmd.Parameters["@PR_P_NO"].Value = PR_P_NO;
                cmd.Parameters.Add("@PR_FIRST_NAME", SqlDbType.Text); cmd.Parameters["@PR_FIRST_NAME"].Value = PR_FIRST_NAME;

                cmd.Parameters.Add("@PR_TERMIN_TYPE", SqlDbType.Text); cmd.Parameters["@PR_TERMIN_TYPE"].Value = PR_TERMIN_TYPE;
                cmd.Parameters.Add("@PR_TERMIN_DATE", SqlDbType.DateTime); cmd.Parameters["@PR_TERMIN_DATE"].Value = PR_TERMIN_DATE;
                cmd.Parameters.Add("@PR_REASONS", SqlDbType.Text); cmd.Parameters["@PR_REASONS"].Value = PR_REASONS;
                cmd.Parameters.Add("@PR_RESIG_RECV", SqlDbType.Text); cmd.Parameters["@PR_RESIG_RECV"].Value = PR_RESIG_RECV;
                cmd.Parameters.Add("@PR_APP_RESIG", SqlDbType.Text); cmd.Parameters["@PR_APP_RESIG"].Value = PR_APP_RESIG;
                cmd.Parameters.Add("@PR_EXIT_INTER", SqlDbType.Text); cmd.Parameters["@PR_EXIT_INTER"].Value = PR_EXIT_INTER;
                cmd.Parameters.Add("@PR_ID_RETURN", SqlDbType.Text); cmd.Parameters["@PR_ID_RETURN"].Value = PR_ID_RETURN;
                cmd.Parameters.Add("@PR_NOTICE", SqlDbType.Text); cmd.Parameters["@PR_NOTICE"].Value = PR_NOTICE;
                cmd.Parameters.Add("@PR_ARTONY", SqlDbType.Text); cmd.Parameters["@PR_ARTONY"].Value = PR_ARTONY;
                cmd.Parameters.Add("@PR_DELETION", SqlDbType.Text); cmd.Parameters["@PR_DELETION"].Value = PR_DELETION;
                cmd.Parameters.Add("@PR_SETTLE", SqlDbType.Text); cmd.Parameters["@PR_SETTLE"].Value = PR_SETTLE;
                cmd.Parameters.Add("@PR_UserID", SqlDbType.Text); cmd.Parameters["@PR_UserID"].Value = PR_UserID;
                //cmd.Parameters.Add("@PR_UploadDate", SqlDbType.Text); cmd.Parameters["@PR_UploadDate"].Value = PR_UploadDate;

                cmd.CommandTimeout = 0;

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        public static DataSet CHRIS_SP_TerminationCheck(string userID)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_Termination_DETAIL]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@userID", SqlDbType.Text);
                cmd.Parameters["@userID"].Value = userID;
                cmd.CommandTimeout = 0;

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        public static DataSet CHRIS_SP_GET_Termin_PRNO(string PR_NO)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();
            int PRNO = Convert.ToInt32(PR_NO);
            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_Termin_GET_PRNO]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PRNO", SqlDbType.Int);
                cmd.Parameters["@PRNO"].Value = PRNO;
                cmd.CommandTimeout = 0;

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        public static void ApprovedTermination(int id)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_Termin_Update_PRNO]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PRNO", SqlDbType.Int);
                cmd.Parameters["@PRNO"].Value = id;
                //cmd.Parameters.Add("@UserID", SqlDbType.Text);
                //cmd.Parameters["@UserID"].Value = UserID;
                cmd.CommandTimeout = 0;

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        public static void ApprovedTerminReject(int id)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_Termin_Reject_Update_PRNO]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PRNO", SqlDbType.Int);
                cmd.Parameters["@PRNO"].Value = id;
                //cmd.Parameters.Add("@UserID", SqlDbType.Text);
                //cmd.Parameters["@UserID"].Value = UserID;
                cmd.CommandTimeout = 0;

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        public static void ExecuteTerminDetailUpload(DataTable dt)
        {
            SqlConnection conn = null;
            try
            {
                conn = connbean.getDatabaseConnection();
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn, SqlBulkCopyOptions.KeepIdentity, null))
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                    }

                    bulkCopy.BulkCopyTimeout = 600;
                    bulkCopy.DestinationTableName = "[dbo].[PERSONNEL_TERMINATION_CHK]";
                    bulkCopy.WriteToServer(dt);
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                //cmd = null;
            }
        }
        #endregion

        #region Increament 

        public static void ExePROMOTION_CHK(DataTable dt)
        {
            SqlConnection conn = null;
            try
            {
                conn = connbean.getDatabaseConnection();
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn, SqlBulkCopyOptions.KeepIdentity, null))
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                    }

                    bulkCopy.BulkCopyTimeout = 600;
                    bulkCopy.DestinationTableName = "[dbo].[INC_PROMOTION_CHK]";
                    bulkCopy.WriteToServer(dt);
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                //cmd = null;
            }
        }

        public static DataSet CHRIS_SP_PROMOTIONCheck(string userID)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_PROMOTION_DETAIL]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@userID", SqlDbType.Text);
                cmd.Parameters["@userID"].Value = userID;
                cmd.CommandTimeout = 0;

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        public static void ApprovedPROMOTION(int id)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_PROMOTION_Update_PRNO]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PRNO", SqlDbType.Int);
                cmd.Parameters["@PRNO"].Value = id;
                //cmd.Parameters.Add("@UserID", SqlDbType.Text);
                //cmd.Parameters["@UserID"].Value = UserID;
                cmd.CommandTimeout = 0;

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        public static void ApprovedIncrementReject(int id)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_PROMOTION_Reject_Update_PRNO]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PRNO", SqlDbType.Int);
                cmd.Parameters["@PRNO"].Value = id;
                //cmd.Parameters.Add("@UserID", SqlDbType.Text);
                //cmd.Parameters["@UserID"].Value = UserID;
                cmd.CommandTimeout = 0;

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        public static DataSet CHRIS_SP_GET_INC_PRO_PRNO(string PR_NO)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();
            int PRNO = Convert.ToInt32(PR_NO);
            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_INC_PROMOTION_GET_PRNO]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PRNO", SqlDbType.Int);
                cmd.Parameters["@PRNO"].Value = PRNO;
                cmd.CommandTimeout = 0;

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }
        #endregion


        #region Segmt and Dept

        public static void ExecuteSegDetailUpload(DataTable dt)
        {
            SqlConnection conn = null;
            try
            {
                conn = connbean.getDatabaseConnection();
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn, SqlBulkCopyOptions.KeepIdentity, null))
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                    }

                    bulkCopy.BulkCopyTimeout = 600;
                    bulkCopy.DestinationTableName = "[dbo].[TRANSFER_CHK]";
                    bulkCopy.WriteToServer(dt);
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                //cmd = null;
            }
        }

        public static DataSet CHRIS_SP_TRANSFER_DETAIL(string userID)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_TRANSFER_DETAIL]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@userID", SqlDbType.Text);
                cmd.Parameters["@userID"].Value = userID;
                cmd.CommandTimeout = 0;

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        public static DataSet CHRIS_SP_GET_SegmtOrDept_PRNO(string PR_NO)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();
            int PRNO = Convert.ToInt32(PR_NO);
            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_PR_SegmtOrDept_DETAIL]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PRNO", SqlDbType.Int);
                cmd.Parameters["@PRNO"].Value = PRNO;
                cmd.CommandTimeout = 0;

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        public static DataSet CHRIS_SP_GET_Dept_PRNO(string PR_NO)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();
            int PRNO = Convert.ToInt32(PR_NO);
            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_PR_Dept_DETAIL]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PRNO", SqlDbType.Int);
                cmd.Parameters["@PRNO"].Value = PRNO;
                cmd.CommandTimeout = 0;

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        public static void ApprovedSegDept(int id)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_SegDept_PRNO]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PRNO", SqlDbType.Int);
                cmd.Parameters["@PRNO"].Value = id;
                //cmd.Parameters.Add("@UserID", SqlDbType.Text);
                //cmd.Parameters["@UserID"].Value = UserID;
                cmd.CommandTimeout = 0;

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        public static void ApprovedSegDeptReject(int id)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_SegDept_Reject_PRNO]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PRNO", SqlDbType.Int);
                cmd.Parameters["@PRNO"].Value = id;
                //cmd.Parameters.Add("@UserID", SqlDbType.Text);
                //cmd.Parameters["@UserID"].Value = UserID;
                cmd.CommandTimeout = 0;

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }
        #endregion

        #region ISFASTLOCAL

        public static void ExecuteISFASTLOCALDetailUpload(DataTable dt)
        {
            SqlConnection conn = null;
            try
            {
                conn = connbean.getDatabaseConnection();
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn, SqlBulkCopyOptions.KeepIdentity, null))
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                    }

                    bulkCopy.BulkCopyTimeout = 900;
                    bulkCopy.DestinationTableName = "[dbo].[PERSONNEL_ISFASTLOCAL_CHK]";
                    bulkCopy.WriteToServer(dt);
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                //cmd = null;
            }
        }

        public static DataSet CHRIS_SP_ISFASTLOCALCheck(string userID)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_ISFASTLOCAL_DETAIL]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@userID", SqlDbType.Text);
                cmd.Parameters["@userID"].Value = userID;
                cmd.CommandTimeout = 0;

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        public static DataSet CHRIS_SP_GET_FastTransfer_PRNO(string PR_NO)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();
            int PRNO = Convert.ToInt32(PR_NO);
            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_FastTransfer_GET_PRNO]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PRNO", SqlDbType.Int);
                cmd.Parameters["@PRNO"].Value = PRNO;
                cmd.CommandTimeout = 0;

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        public static void ApprovedFastLocalReject(int id)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_ISFASTLOCAL_Reject_Update_PRNO]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PRNO", SqlDbType.Int);
                cmd.Parameters["@PRNO"].Value = id;
                //cmd.Parameters.Add("@UserID", SqlDbType.Text);
                //cmd.Parameters["@UserID"].Value = UserID;
                cmd.CommandTimeout = 0;

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        public static void ApprovedFastLocal(int id)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_ISFASTLOCAL_Update_PRNO]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PRNO", SqlDbType.Int);
                cmd.Parameters["@PRNO"].Value = id;
                //cmd.Parameters.Add("@UserID", SqlDbType.Text);
                //cmd.Parameters["@UserID"].Value = UserID;
                cmd.CommandTimeout = 0;

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }
        #endregion


        #region Personnel 

        public static void ExecutePersonnelDetailUpload(DataTable dt)
        {
            SqlConnection conn = null;
            try
            {
                conn = connbean.getDatabaseConnection();
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn, SqlBulkCopyOptions.KeepIdentity, null))
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                    }

                    bulkCopy.BulkCopyTimeout = 600;
                    bulkCopy.DestinationTableName = "[dbo].[PERSONNEL_CHK]";
                    bulkCopy.WriteToServer(dt);
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                //cmd = null;
            }
        }

        public static void ExecutePersonnelEmpDetailUpload(DataTable dt)
        {
            SqlConnection conn = null;
            try
            {
                conn = connbean.getDatabaseConnection();
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn, SqlBulkCopyOptions.KeepIdentity, null))
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                    }

                    bulkCopy.BulkCopyTimeout = 600;
                    bulkCopy.DestinationTableName = "[dbo].[PRE_EMP]";
                    bulkCopy.WriteToServer(dt);
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                //cmd = null;
            }
        }

        public static void ExecutePersonnelEDUDetailUpload(DataTable dt)
        {
            SqlConnection conn = null;
            try
            {
                conn = connbean.getDatabaseConnection();
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn, SqlBulkCopyOptions.KeepIdentity, null))
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                    }

                    bulkCopy.BulkCopyTimeout = 600;
                    bulkCopy.DestinationTableName = "[dbo].[EDUCATION]";
                    bulkCopy.WriteToServer(dt);
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                //cmd = null;
            }
        }

        public static void ExecutePersonnelChildUpload(DataTable dt)
        {
            SqlConnection conn = null;
            try
            {
                conn = connbean.getDatabaseConnection();
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn, SqlBulkCopyOptions.KeepIdentity, null))
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                    }

                    bulkCopy.BulkCopyTimeout = 600;
                    bulkCopy.DestinationTableName = "[dbo].[CHILDREN]";
                    bulkCopy.WriteToServer(dt);
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                //cmd = null;
            }
        }

        //ExecutePersonnelUpload
        public static void ExecutePersonnelUpload(DataTable dt)
        {
            SqlConnection conn = null;
            try
            {
                conn = connbean.getDatabaseConnection();
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn, SqlBulkCopyOptions.KeepIdentity, null))
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                    }

                    bulkCopy.BulkCopyTimeout = 600;
                    bulkCopy.DestinationTableName = "[dbo].[PERSONAL]";
                    bulkCopy.WriteToServer(dt);
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                //cmd = null;
            }
        }
        #endregion

        #region Final Settlement

        public static DataSet CHRIS_SP_PF_YEAR_WISE(int PRNO)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_GET_PR_PF_YEAR_WISE]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PRNO", SqlDbType.Int);
                cmd.Parameters["@PRNO"].Value = PRNO;
                cmd.CommandTimeout = 0;

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        public static DataSet CHRIS_SP_PF_YEAR_WISE_DETAIL(int PRNO)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_GET_PR_PF_YEAR_WISE_DETAIL]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PRNO", SqlDbType.Int);
                cmd.Parameters["@PRNO"].Value = PRNO;
                cmd.CommandTimeout = 0;

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }


        public static DataSet CHRIS_SP_PF_YEAR_WISE_DETAIL_PFI_RATE()
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_GET_PR_PF_YEAR_WISE_PFI_RATE]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.Add("@PRNO", SqlDbType.Int);
                //cmd.Parameters["@PRNO"].Value = PRNO;
                cmd.CommandTimeout = 0;

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        public static DataSet CHRIS_SP_PF_YEAR_WISE_DETAIL_PFI_PF(int PRNO)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_GET_PR_PF_YEAR_WISE_PFI_PF]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PRNO", SqlDbType.Int);
                cmd.Parameters["@PRNO"].Value = PRNO;
                cmd.CommandTimeout = 0;

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }


        public static DataSet CHRIS_GET_PR_INFO_FOR_SETTLEMENT(int PRNO)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[CHRIS_GET_PR_INFO_FOR_SETTLEMENT]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PRNO", SqlDbType.Int);
                cmd.Parameters["@PRNO"].Value = PRNO;
                cmd.CommandTimeout = 0;

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }
        #endregion
        #region --New Methods--
        /// <summary>
        /// Batch Update
        /// </summary>
        /// <param name="dstParams"></param>
        /// <param name="poDT"></param>
        /// <CreatedBy>S.Haider Raza</CreatedBy>
        /// <returns></returns>
        public static Result ExecuteBatchUpdate(DataSet dstParams, DataTable poDT)
        {
            Result rslt = new Result();
            SqlCommand cmdInsert = null;
            SqlCommand cmdUpdate = null;
            SqlCommand cmdDel = null;
            SqlTransaction trans = null;
            SqlConnection conn = null;
            SqlDataAdapter adpt = null;

            rslt.isSuccessful = false;
            rslt.hstOutParams = new Hashtable();

            try
            {
                conn = connbean.getDatabaseConnection();
                //trans = conn.BeginTransaction();

                foreach (DataTable dtlParams in dstParams.Tables)
                {
                    cmdInsert = new SqlCommand(dtlParams.TableName, conn, trans);
                    cmdUpdate = new SqlCommand(dtlParams.TableName, conn, trans);
                    cmdDel = new SqlCommand(dtlParams.TableName, conn, trans);

                    cmdInsert.CommandType = CommandType.StoredProcedure;
                    cmdInsert.UpdatedRowSource = UpdateRowSource.OutputParameters;
                    cmdUpdate.CommandType = CommandType.StoredProcedure;
                    cmdUpdate.UpdatedRowSource = UpdateRowSource.None;
                    cmdDel.CommandType = CommandType.StoredProcedure;
                    cmdDel.UpdatedRowSource = UpdateRowSource.None;

                    CreateCommandParamsForBatch(ref cmdInsert, dstParams.Tables[0]);
                    CreateCommandParamsForBatch(ref cmdUpdate, dstParams.Tables[0]);
                    CreateCommandParamsForBatch(ref cmdDel, dstParams.Tables[0]);

                    if (cmdInsert.Parameters.Contains(ActionType))
                        cmdInsert.Parameters[ActionType].Value =
                            Enum.GetName(typeof(Enumerations.eActionType), Enumerations.eActionType.Save);
                    else
                        cmdInsert.Parameters.Add(new SqlParameter(ActionType,
                            Enum.GetName(typeof(Enumerations.eActionType), Enumerations.eActionType.Save)));

                    if (cmdUpdate.Parameters.Contains(ActionType))
                        cmdUpdate.Parameters[ActionType].Value =
                            Enum.GetName(typeof(Enumerations.eActionType), Enumerations.eActionType.Update);
                    else
                        cmdUpdate.Parameters.Add(new SqlParameter(ActionType,
                            Enum.GetName(typeof(Enumerations.eActionType), Enumerations.eActionType.Update)));

                    if (cmdDel.Parameters.Contains(ActionType))
                        cmdDel.Parameters[ActionType].Value =
                            Enum.GetName(typeof(Enumerations.eActionType), Enumerations.eActionType.Delete);
                    else
                        cmdDel.Parameters.Add(new SqlParameter(ActionType,
                            Enum.GetName(typeof(Enumerations.eActionType), Enumerations.eActionType.Delete)));

                    adpt = new SqlDataAdapter();
                    adpt.InsertCommand = cmdInsert;
                    adpt.UpdateCommand = cmdUpdate;
                    adpt.DeleteCommand = cmdDel;
                    rslt.objResult = adpt.Update(poDT);
                }

                //trans.Commit();
                rslt.isSuccessful = true;

                // Comment by SL, need to be enabled later.
                //LogDMLAcitivities(dstParams);
            }
            catch (Exception ex)
            {
                //trans.Rollback();

                rslt.isSuccessful = false;
                rslt.exp = ex;
                rslt.message = "Error in updating batch records!" + ex.Message;

                LogException("SQLManager", "ExecuteDML", ex);
            }
            finally
            {
                connbean.closeConn(conn);

                conn = null;
                cmdInsert = null;
                cmdUpdate = null;
                cmdDel = null;
                trans = null;
            }

            return rslt;
        }

        /// <summary>
        /// CreateCommandParamsForBatch
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="dtlParams"></param>
        private static void CreateCommandParamsForBatch(ref SqlCommand cmd, DataTable dtlParams)
        {
            SqlParameter parameter = null;

            try
            {
                cmd.Parameters.Clear();

                foreach (DataColumn dcl in dtlParams.Columns)
                {
                    parameter = new SqlParameter(dcl.ColumnName, GetDBType(dcl.DataType.ToString()));
                    if (dcl.ColumnName != ActionType)
                        parameter.SourceColumn = dcl.ColumnName;

                    if (Convert.ToBoolean(dcl.ExtendedProperties["IsOutParam"]))
                        parameter.Direction = ParameterDirection.InputOutput;
                    else
                        parameter.Direction = ParameterDirection.Input;

                    cmd.Parameters.Add(parameter);
                }

            }
            catch (Exception ex)
            {
                LogException("SQLManager", "CreateCommandParamsForBatch", ex);
            }
        }

        #endregion

        #region Income Tax Additional Field Uploader

        public static void ExecuteIncomeTaxAdditionalFieldUploader(DataTable dt)
        {
            SqlConnection conn = null;
            try
            {
                conn = connbean.getDatabaseConnection();
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn, SqlBulkCopyOptions.KeepIdentity, null))
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                    }

                    bulkCopy.BulkCopyTimeout = 600;
                    bulkCopy.DestinationTableName = "[dbo].[TEMP_INCOMETAX_FUND]";
                    bulkCopy.WriteToServer(dt);
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                //cmd = null;
            }
        }

        public static DataSet ExecuteINCOMETAX_FUNDData()
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[SP_getINCOMETAX_FUND]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.Add("@UserID", SqlDbType.Text);
                //cmd.Parameters["@UserID"].Value = UserID;
                cmd.CommandTimeout = 0;
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }

        public static DataSet ExecuteFindEmployee(DateTime frmDate, DateTime toDate, int EmpNo)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("[dbo].[SP_FindEmployee]", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@frmDate", SqlDbType.DateTime);
                cmd.Parameters["@frmDate"].Value = frmDate;

                cmd.Parameters.Add("@toDate", SqlDbType.DateTime);
                cmd.Parameters["@toDate"].Value = toDate;

                cmd.Parameters.Add("@EmpNo", SqlDbType.Int);
                cmd.Parameters["@EmpNo"].Value = EmpNo;

                cmd.CommandTimeout = 0;
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                LogException("SQLManager", "Automation Reports SP fail to delete Uni Number", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }


        public static int ExecuteManualUpdateBonusEntry(decimal BO_INC_AMOUNT, string BO_INC_APP, decimal BO_HL_MARKUP, decimal BO_Investment, decimal BO_Donation, decimal BO_Utilities, int ID, int BO_P_NO)
        {
            SqlConnection conn = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                conn = connbean.getDatabaseConnection();
                cmd = new SqlCommand("UPDATE dbo.BONUS_TAB SET BO_INC_AMOUNT = @BO_INC_AMOUNT ,BO_INC_APP = @BO_INC_APP ,BO_HL_MARKUP = @BO_HL_MARKUP ,BO_Investment = @BO_Investment ,BO_Donation = @BO_Donation,BO_Utilities = @BO_Utilities Where 	 ID = @ID AND BO_P_NO = @BO_P_NO", conn);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@BO_INC_AMOUNT", SqlDbType.Decimal);
                cmd.Parameters["@BO_INC_AMOUNT"].Value = BO_INC_AMOUNT;

                cmd.Parameters.Add("@BO_INC_APP", SqlDbType.VarChar);
                cmd.Parameters["@BO_INC_APP"].Value = BO_INC_APP;

                cmd.Parameters.Add("@BO_HL_MARKUP", SqlDbType.Decimal);
                cmd.Parameters["@BO_HL_MARKUP"].Value = BO_HL_MARKUP;

                cmd.Parameters.Add("@BO_Investment", SqlDbType.Decimal);
                cmd.Parameters["@BO_Investment"].Value = BO_Investment;

                cmd.Parameters.Add("@BO_Donation", SqlDbType.Decimal);
                cmd.Parameters["@BO_Donation"].Value = BO_Donation;

                cmd.Parameters.Add("@BO_Utilities", SqlDbType.Decimal);
                cmd.Parameters["@BO_Utilities"].Value = BO_Utilities;

                cmd.Parameters.Add("@ID", SqlDbType.Int);
                cmd.Parameters["@ID"].Value = ID;

                cmd.Parameters.Add("@BO_P_NO", SqlDbType.Decimal);
                cmd.Parameters["@BO_P_NO"].Value = BO_P_NO;
                cmd.CommandTimeout = 0;

                return cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                LogException("SQLManager", "SP ExecuteManualUpdateBonusEntry fail to EXECUTE with exception ", ex);
                throw ex;
            }
            finally
            {
                connbean.closeConn(conn);
                conn = null;
                cmd = null;
            }
        }


        #endregion
    }
}
