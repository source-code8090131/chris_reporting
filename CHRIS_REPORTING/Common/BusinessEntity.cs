﻿using System.Data;

namespace CHRIS_REPORTING.Common
{
    public class BusinessEntity : IEntityCommand
    {

        #region --Field Segment--
        private const string m_strParamPreFix = "@";
        private const string m_strActionType = "ActionType";
        private string m_strBusinessEntityNamespace = "iCORE.";
        private string searchFilter;
        private string soeId;

        public string SoeId
        {
            get { return soeId; }
            set { soeId = value; }
        }

        public string SearchFilter
        {
            get { return searchFilter; }
            set { searchFilter = value; }
        }


        #endregion

    }
}
