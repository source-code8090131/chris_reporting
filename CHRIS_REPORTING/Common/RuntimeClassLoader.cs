using System;
using System.Configuration;
using System.Reflection;
using System.Data;
using iCORE.Common;
using CHRIS_REPORTING.Common;

/*****************************************************************
  Class Name:  RuntimeClassLoader 
  Class Description: <A brief summary of what is the purpose of the class>
  Created By: Shamraiz Ahmad
  Created Date: 22-08-2007
  Version No: 1.0
  Modification: <Mention the modification in the class >               
  Modified By:   
  Modification Date:
  Version No: <The version No after modification>

*****************************************************************/

namespace iCORE.Common
{
    public class RuntimeClassLoader
    {
        #region --Systems Limited--
        private const string m_strBusinessEntityNamespace = "iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.";

        /// <summary>
        /// GetBusinessEntity
        /// </summary>
        /// <param name="pstrEntityName"></param>
        /// <returns></returns>
        public static BusinessEntity GetBusinessEntity(string pstrEntityName)
        {
            BusinessEntity oEntityClass =
                Activator.CreateInstance(Type.GetType(pstrEntityName)) as BusinessEntity;

            return oEntityClass;
        }

        #endregion

        /// <summary>
        /// GetEntity
        /// </summary>
        /// <param name="entityName"></param>
        /// <returns></returns>
        public static IEntityCommand GetEntity(string entityName)
        {
            IEntityCommand entity =
                Activator.CreateInstance(Type.GetType(
                entityName + "Command")) as IEntityCommand;

            return entity;
        }

        /// <summary>
        /// GetDataManager
        /// </summary>
        /// <param name="dataManagerName"></param>
        /// <returns></returns>
        public static IDataManager GetDataManager(string dataManagerName)
        {
            IDataManager dataManager =
                Activator.CreateInstance(Type.GetType(
                ((dataManagerName == null || dataManagerName == string.Empty) ? "iCORE.Common.CommonDataManager" : dataManagerName))) as IDataManager;

            return dataManager;
        }

        /// <summary>
        /// GetStructures
        /// </summary>
        /// <param name="dataManagerNames"></param>
        /// <param name="oprtnType"></param>
        /// <returns></returns>
        public static DataSet GetStructures(string [] dataManagerNames, OperationType  oprtnType)
        {
            IDataManager[] dataManagers = new IDataManager[dataManagerNames.Length];
            DataSet dstSturctures = new DataSet();

            for (int i = 0; i < dataManagers.Length; i++)
            {
                dataManagers[i] = Activator.CreateInstance(Type.GetType(
                    dataManagerNames[i] + "DataManager")) as IDataManager;

                dstSturctures.Tables.Add(dataManagers[i].GetStructure(oprtnType));
            }

            return dstSturctures;
        }

        /// <summary>
        /// GetStructures
        /// </summary>
        /// <param name="dataManagerNames"></param>
        /// <param name="postfix"></param>
        /// <returns></returns>
        public static DataSet GetStructures(string[] dataManagerNames, string postfix)
        {
            DataSet dstSturctures = new DataSet();

            for (int i = 0; i < dataManagerNames.Length; i++)
            {
                dstSturctures.Tables.Add(SQLManager.GetSPParams(StoredProcedureList.SP_PREFIX + dataManagerNames[i] + "_" + postfix));
            }

            return dstSturctures;
        }
    }
}
