﻿using CHRIS_REPORTING.Services;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

namespace CHRIS_REPORTING.Common
{
    public class Basereporting
    {
        public Stream GetReportWithSubReportCustom<T>(T entity, string rptFileName) where T : class
        {
            string responseMessage = "";
            CrystalDecisions.CrystalReports.Engine.ReportDocument Temp;
            string m_rptFilePath = Path.Combine(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath, "Reports\\" + rptFileName + ".rpt");
            ConnectionBean bean = new ConnectionBean();
            SQLManager.SetConnectionBean(bean);

            string m_SPName = "CHRIS_RPT_SP_" + rptFileName;
            DataTable dataTable = new DataTable();
            dataTable.TableName = m_SPName;

            dataTable = SQLManager.GetSPParams(m_SPName);
            dataTable.Rows.Add();
            ReportDocument crDoc = new ReportDocument();
            crDoc.Load(m_rptFilePath);
            string[] Credentials = { bean.DbName, bean.M_ServerName };
            ConnectionInfo conn = new ConnectionInfo();
            conn.DatabaseName = bean.DbName;
            conn.ServerName = bean.M_ServerName;
            DoCrystalReportLogin(crDoc, Credentials);
            DataSet ds = new DataSet();
            ds.Tables.Clear();
            ds.Tables.Add(dataTable);
            foreach (var prop in typeof(T).GetProperties())
            {
                if (dataTable.Columns.Contains(prop.Name))
                {
                    var value = prop.GetValue(entity);
                    dataTable.Rows[0][prop.Name] = value != null ? value : DBNull.Value;
                }
            }
            Result result = SQLManager.SelectDataSet(ds);
            if (result.isSuccessful)
            {
                if (result.dstResult.Tables.Count > 0)
                {
                    foreach (DataTable table in result.dstResult.Tables)
                    {
                        string tableName = table.TableName;
                        string CCrtableName = table.TableName;
                        bool tableExists = false;
                        // Check if the table exists in the report's database tables collection
                        foreach (Table crTable in crDoc.Database.Tables)
                        {
                            string ReportSimpleTablename = crTable.Name.Substring(0, crTable.Name.IndexOf(";"));
                            if (ReportSimpleTablename == tableName)
                            {
                                tableExists = true;
                                CCrtableName = crTable.Name;
                                break;
                            }
                        }

                        if (tableExists)
                        {
                            Table crTable = crDoc.Database.Tables[CCrtableName];
                            // Set the data source for the Crystal Report table
                            crTable.SetDataSource(table);
                        }
                        else
                        {
                            // Handle if the table doesn't exist in the report
                            // You might log or handle this situation according to your requirements
                            responseMessage = "Table {" + tableName + "} not found in the report.";
                        }
                    }
                    //crDoc.SetDataSource(new[] { result.dstResult });
                    crDoc.Refresh();
                    //Stream ReportFileStream = crDoc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    //string DateTimeNow = DateTime.Now.ToString("HHmmss");
                    //string SavingPath = Path.Combine(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath, "TesOutputFolder\\" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf");
                    //using (var fileStream = new FileStream(SavingPath, FileMode.Create, FileAccess.Write))
                    //{
                    //    ReportFileStream.CopyTo(fileStream);
                    //}
                    //crDoc.Close();
                    //crDoc.Dispose();

                    if (responseMessage != "")
                        return null;
                    Stream stream = crDoc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return stream;
                }
            }

            return null;
        }
        public bool DoCrystalReportLogin(CrystalDecisions.CrystalReports.Engine.ReportDocument rptDoc, string[] Credentials)
        {

            TableLogOnInfo conInfo = new TableLogOnInfo();

            bool testReturn = true;
            conInfo.ConnectionInfo.DatabaseName = Credentials[0];
            conInfo.ConnectionInfo.ServerName = Credentials[1];
            conInfo.ConnectionInfo.IntegratedSecurity = true;

            foreach (CrystalDecisions.CrystalReports.Engine.Table tbl in rptDoc.Database.Tables)
            {
                tbl.ApplyLogOnInfo(conInfo);
                if (!tbl.TestConnectivity())
                {
                    testReturn = false;
                    break;
                }

                if (tbl.Location.IndexOf(".") > 0)
                {
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1);
                }
                else
                {
                    tbl.Location = tbl.Location;
                }

            }

            if (testReturn && rptDoc.ReportDefinition.ReportObjects.Count > 0)
            {
                int index, count, counter;
                SubreportObject mySubReportObject;
                ReportDocument mySubRepDoc;

                for (index = 0; index <= rptDoc.ReportDefinition.Sections.Count - 1; index++)
                {
                    for (count = 0; count <= rptDoc.ReportDefinition.Sections[index].ReportObjects.Count - 1; count++)
                    {
                        Section rptSection = rptDoc.ReportDefinition.Sections[index];
                        if (rptSection.ReportObjects[count].Kind == ReportObjectKind.SubreportObject)
                        {
                            mySubReportObject = (SubreportObject)rptSection.ReportObjects[count];
                            mySubRepDoc = mySubReportObject.OpenSubreport(mySubReportObject.SubreportName);
                            for (counter = 0; counter <= mySubRepDoc.Database.Tables.Count - 1; counter++)
                            {
                                mySubRepDoc.Database.Tables[counter].ApplyLogOnInfo(conInfo);
                                if (!mySubRepDoc.Database.Tables[counter].TestConnectivity())
                                {
                                    testReturn = false;
                                    break;
                                }

                            }

                        }
                    }

                }

            }

            return testReturn;

        }
    }
}