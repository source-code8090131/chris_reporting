﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace CHRIS_REPORTING.Common
{
    public enum OperationType
    {
        Add = 0,
        Update = 1,
        Delete = 2,
        Get = 3,
        GetAll = 4,
        GetAllByParams = 5,
        Duplicate = 6,
        GetAllAuthorized = 7,
        GetAllUnAuthorized = 8,
        GetByCode = 9,
        Authorize = 10,
        UnAuthorize = 11,
        NameDuplicate = 12,
        AbbreviationDuplicate = 13,
        DescriptionDuplicate = 14,
        DateDuplicate = 15,
        GetByAbbreviation = 16
    }

    public struct Result
    {
        public DataSet dstResult;
        public Hashtable hstOutParams;
        public IEntityCommand entityResult;
        public object objResult;
        public Exception exp;
        public string message;
        public bool isSuccessful;
    }

    public interface IDataManager
    {
        Result Execute(DataSet dst);
        //Result Add(IEntityCommand entity);
        Result Add(IEntityCommand entity, string pstrSPName, string pstrActionType); // SL Added
        //Result Update(IEntityCommand entity);
        Result Update(IEntityCommand entity, string pstrSPName, string pstrActionType); // SL Added
        Result BatchUpdate(IEntityCommand entity, string pstrSPName, DataTable poDT); // SL Added
        //Result Delete(IEntityCommand entity);
        Result Delete(IEntityCommand entity, string pstrSPName, string pstrActionType); // SL Added
        Result GetAll(BusinessEntity poEntity, string pstrSPName, string pstrActionType); // SL Added
        Result GetAll(BusinessEntity poEntity, string pstrSPName, string userID, string pstrActionType); // SL New Added
        Result GetAll(BusinessEntity poEntity, string pstrSPName, string pstrActionType, ref SqlTransaction trans); // SL Added
        Result GetAll(string pstrSPName, string pstrActionType, string pstrSearchText, string pstrSearchColumn); // SL Added
        Result GetAll(string pstrSPName, string pstrActionType, string pstrSearchText, string pstrSearchColumn, string searchFilter); // SL Added
        Result GetAll(IEntityCommand entity);
        Result GetAll();
        Result GetAllAuthorized(params object[] values);
        Result GetAllUnAuthorized(params object[] values);
        Result Get(IEntityCommand entity);
        Result GetByCode(string code);
        Result GetByCode(int masterId, string code);
        Result GetByAbbreviation(int masterId, string Abbreviation);
        //Result GetEntityCommand(IEntityCommand entity);
        Result IsDuplicate(int id, string code);
        Result IsDuplicate(int masterId, int id, string code);
        Result IsNameDuplicate(int id, string code);
        Result IsNameDuplicate(int masterId, int id, string code);
        Result IsAbbreviationDuplicate(int id, string code);
        Result IsAbbreviationDuplicate(int masterId, int id, string code);
        Result IsDescriptionDuplicate(int id, string desc);
        Result IsDescriptionDuplicate(int masterId, int id, string desc);
        Result IsDateDuplicate(int id, string date);
        Result IsDateDuplicate(int masterId, int id, string date);
        Result Authorize(int id, string userId, DateTime time);
        Result UnAuthorize(int id);
        DataTable GetStructure(OperationType oprtnType);

    }
}
