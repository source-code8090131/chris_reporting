﻿using iCORE.Common;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Reflection;

namespace CHRIS_REPORTING.Common
{
    public abstract class DataManager : IDataManager
    {
        protected internal string spName;
        private const string m_strActionType = "ActionType";

        /// <summary>
        /// LogException
        /// </summary>
        /// <param name="exceptionLogEntity"></param>
        //public static void LogException(ExceptionLogCommand exceptionLogEntity)
        //{
        //    LogException(exceptionLogEntity.ClassName, exceptionLogEntity.FunctionName,
        //        exceptionLogEntity.Exception);
        //}

        /// <summary>
        /// GetByAbbreviation
        /// </summary>
        /// <param name="masterId"></param>
        /// <param name="Abbreviation"></param>
        /// <returns></returns>
        public Result GetByAbbreviation(int masterId, string Abbreviation)
        {
            DataSet dst = new DataSet();
            DataRow drw = null;

            this.SetSPName(OperationType.GetByAbbreviation);

            dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            drw = dst.Tables[0].NewRow();

            drw["MasterID"] = masterId;
            drw["Abbreviation"] = Abbreviation;

            dst.Tables[0].Rows.Add(drw);
            dst.Tables[0].AcceptChanges();

            return SQLManager.SelectDataSet(dst);
        }

        /// <summary>
        /// IsAbbreviationDuplicate
        /// </summary>
        /// <param name="id"></param>
        /// <param name="abbr"></param>
        /// <returns></returns>
        public Result IsAbbreviationDuplicate(int id, string abbr)
        {
            DataTable dtl = new DataTable();
            DataRow drw = null;

            this.SetSPName(OperationType.AbbreviationDuplicate);

            dtl = SQLManager.GetSPParams(this.spName);

            drw = dtl.NewRow();

            drw["ID"] = id;
            drw["Abbreviation"] = abbr;

            dtl.Rows.Add(drw);
            dtl.AcceptChanges();

            return SQLManager.SelectScaler(dtl);
        }

        /// <summary>
        /// IsAbbreviationDuplicate
        /// </summary>
        /// <param name="masterId"></param>
        /// <param name="id"></param>
        /// <param name="abbr"></param>
        /// <returns></returns>
        public Result IsAbbreviationDuplicate(int masterId, int id, string abbr)
        {
            DataTable dtl = new DataTable();
            DataRow drw = null;

            this.SetSPName(OperationType.AbbreviationDuplicate);

            dtl = SQLManager.GetSPParams(this.spName);

            drw = dtl.NewRow();

            drw["ID"] = id;
            drw["MasterID"] = masterId;
            drw["Abbreviation"] = abbr;

            dtl.Rows.Add(drw);
            dtl.AcceptChanges();

            return SQLManager.SelectScaler(dtl);
        }

        /// <summary>
        /// IsDescriptionDuplicate
        /// </summary>
        /// <param name="id"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public Result IsDescriptionDuplicate(int id, string description)
        {
            DataTable dtl = new DataTable();
            DataRow drw = null;

            this.SetSPName(OperationType.DescriptionDuplicate);

            dtl = SQLManager.GetSPParams(this.spName);

            drw = dtl.NewRow();

            drw["ID"] = id;
            drw["Description"] = description;

            dtl.Rows.Add(drw);
            dtl.AcceptChanges();

            return SQLManager.SelectScaler(dtl);
        }

        /// <summary>
        /// IsDescriptionDuplicate
        /// </summary>
        /// <param name="masterId"></param>
        /// <param name="id"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public Result IsDescriptionDuplicate(int masterId, int id, string description)
        {
            DataTable dtl = new DataTable();
            DataRow drw = null;

            this.SetSPName(OperationType.DescriptionDuplicate);

            dtl = SQLManager.GetSPParams(this.spName);

            drw = dtl.NewRow();

            drw["ID"] = id;
            drw["MasterID"] = masterId;
            drw["Description"] = description;

            dtl.Rows.Add(drw);
            dtl.AcceptChanges();

            return SQLManager.SelectScaler(dtl);
        }

        /// <summary>
        /// IsDateDuplicate
        /// </summary>
        /// <param name="id"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public Result IsDateDuplicate(int id, string date)
        {
            DataTable dtl = new DataTable();
            DataRow drw = null;

            this.SetSPName(OperationType.DateDuplicate);

            dtl = SQLManager.GetSPParams(this.spName);

            drw = dtl.NewRow();

            drw["ID"] = id;
            drw["Date"] = date;

            dtl.Rows.Add(drw);
            dtl.AcceptChanges();

            return SQLManager.SelectScaler(dtl);
        }

        /// <summary>
        /// IsDateDuplicate
        /// </summary>
        /// <param name="masterId"></param>
        /// <param name="id"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public Result IsDateDuplicate(int masterId, int id, string date)
        {
            DataTable dtl = new DataTable();
            DataRow drw = null;

            this.SetSPName(OperationType.DateDuplicate);

            dtl = SQLManager.GetSPParams(this.spName);

            drw = dtl.NewRow();

            drw["ID"] = id;
            drw["MasterID"] = masterId;
            drw["Date"] = date;

            dtl.Rows.Add(drw);
            dtl.AcceptChanges();

            return SQLManager.SelectScaler(dtl);
        }

        /// <summary>
        /// LogException
        /// </summary>
        /// <param name="className"></param>
        /// <param name="functionName"></param>
        /// <param name="exp"></param>
        public static void LogException(string className, string functionName, Exception exp)
        {
            SQLManager.LogException(className, functionName, exp);
            // LogException(exceptionLogEntity.ClassName, exceptionLogEntity.FunctionName,exceptionLogEntity.Exception);
        }

        public static void LogException(string className, string functionName, ExceptionLogCommand exceptionLogEntity)
        {
            LogException(exceptionLogEntity.ClassName, exceptionLogEntity.FunctionName,exceptionLogEntity.Exception);
        }

        /// <summary>
        /// ExecuteDataSet
        /// </summary>
        /// <param name="dst"></param>
        /// <returns></returns>
        public static Result ExecuteDataSet(DataSet dst)
        {
            return SQLManager.ExecuteDML(dst);
        }

        /// <summary>
        /// Execute
        /// </summary>
        /// <param name="dst"></param>
        /// <returns></returns>
        public virtual Result Execute(DataSet dst)
        {
            return SQLManager.ExecuteDML(dst);
        }

        // Added by SL
        /// <summary>
        /// Add
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="pstrSPName"></param>
        /// <param name="pstrActionType"></param>
        /// <returns></returns>
        public virtual Result Add(IEntityCommand entity, string pstrSPName, string pstrActionType)
        {
            DataSet dst = new DataSet();
            this.spName = pstrSPName;

            dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            this.PopulateDataTableFromObject(entity, dst.Tables[this.spName], pstrActionType);

            return SQLManager.ExecuteDML(dst);
        }

        // Added by SL
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="pstrSPName"></param>
        /// <param name="pstrActionType"></param>
        /// <returns></returns>
        public virtual Result Update(IEntityCommand entity, string pstrSPName, string pstrActionType)
        {
            DataSet dst = new DataSet();

            this.spName = pstrSPName;

            dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            this.PopulateDataTableFromObject(entity, dst.Tables[this.spName], pstrActionType);

            return SQLManager.ExecuteDML(dst);
        }

        // Added by SL
        /// <summary>
        /// PopulateDataTableFromObject
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="dtl"></param>
        /// <param name="pstrActionType"></param>
        public virtual void PopulateDataTableFromObject(IEntityCommand entity, DataTable dtl, string pstrActionType)
        {
            DataRow drw = null;
            drw = dtl.NewRow();

            if (pstrActionType != string.Empty)
            {
                if (dtl.Columns.Contains(m_strActionType))
                    drw[m_strActionType] = pstrActionType;
            }

            PropertyInfo[] oEntityProperties = entity.GetType().GetProperties();

            foreach (PropertyInfo oLoopProperty in oEntityProperties)
            {
                if (oLoopProperty != null && oLoopProperty.CanRead)
                {
                    if (oLoopProperty.PropertyType == typeof(DateTime))
                    {
                        DateTime oDateTime;
                        if (DateTime.TryParse(oLoopProperty.GetValue(entity, null).ToString(), out oDateTime))
                        {

                            if (dtl.Columns.Contains(oLoopProperty.Name))
                            {
                                if (!(DateTime.Equals(Convert.ToDateTime(oLoopProperty.GetValue(entity, null)), DateTime.MinValue)))
                                    drw[oLoopProperty.Name] = oLoopProperty.GetValue(entity, null) == null ? DBNull.Value : oLoopProperty.GetValue(entity, null);//oLoopProperty.GetValue(entity, null);
                            }
                            //if (poDbCmd.Parameters.Contains(m_strParamPreFix + oLoopProperty.Name))
                            //    poDbCmd.Parameters[m_strParamPreFix + oLoopProperty.Name].Value = oLoopProperty.GetValue(this, null);
                        }
                    }
                    /*Added By Umair 21 April 2011*/
                    else if (oLoopProperty.PropertyType == typeof(String))
                    {
                        if (dtl.Columns.Contains(oLoopProperty.Name))
                            drw[oLoopProperty.Name] = string.IsNullOrEmpty(Convert.ToString(oLoopProperty.GetValue(entity, null))) ? DBNull.Value : oLoopProperty.GetValue(entity, null);
                    }
                    /*------Added By Umair 21 April 2011------*/
                    else
                    {
                        if (dtl.Columns.Contains(oLoopProperty.Name))
                            drw[oLoopProperty.Name] = oLoopProperty.GetValue(entity, null) == null ? DBNull.Value : oLoopProperty.GetValue(entity, null);

                    }
                }
            }

            dtl.Rows.Add(drw);
            dtl.AcceptChanges();
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="pstrSPName"></param>
        /// <param name="pstrActionType"></param>
        /// <returns></returns>
        public virtual Result Delete(IEntityCommand entity, string pstrSPName, string pstrActionType)
        {
            DataSet dst = new DataSet();
            this.spName = pstrSPName;

            dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            this.PopulateDataTableFromObjectForDeletion(entity, dst.Tables[this.spName], pstrActionType);

            return SQLManager.ExecuteDML(dst);
        }

        // Added by SL
        /// <summary>
        /// PopulateDataTableFromObjectForDeletion
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="dtl"></param>
        /// <param name="pstrActionType"></param>
        public virtual void PopulateDataTableFromObjectForDeletion(IEntityCommand entity, DataTable dtl, string pstrActionType)
        {
            DataRow drw = null;
            drw = dtl.NewRow();

            if (pstrActionType != string.Empty)
            {
                if (dtl.Columns.Contains(m_strActionType))
                    drw[m_strActionType] = pstrActionType;
            }

            PropertyInfo oEntityProperty = entity.GetType().GetProperty("ID");

            if (oEntityProperty != null && oEntityProperty.CanRead)
            {
                if (oEntityProperty.PropertyType == typeof(DateTime))
                {
                    DateTime oDateTime;
                    if (DateTime.TryParse(oEntityProperty.GetValue(entity, null).ToString(), out oDateTime))
                    {
                        if (dtl.Columns.Contains(oEntityProperty.Name))
                            drw[oEntityProperty.Name] = drw[oEntityProperty.Name] = oEntityProperty.GetValue(entity, null) == null ? DBNull.Value : oEntityProperty.GetValue(entity, null);//oEntityProperty.GetValue(entity, null);
                    }
                }
                else
                {
                    if (dtl.Columns.Contains(oEntityProperty.Name))
                        drw[oEntityProperty.Name] = oEntityProperty.GetValue(entity, null) == null ? DBNull.Value : oEntityProperty.GetValue(entity, null);//oEntityProperty.GetValue(entity, null);
                }
            }

            dtl.Rows.Add(drw);
            dtl.AcceptChanges();
        }

        /// <summary>
        /// Added by Systems Limited For LOV
        /// </summary>
        /// <param name="dtl"></param>
        /// <param name="pstrActionType"></param>
        /// <param name="pstrSearchText"></param>
        /// <param name="pstrSearchColumn"></param>
        public virtual void PopulateDataTableFromObject(DataTable dtl, string pstrActionType, string pstrSearchText, string pstrSearchColumn)
        {
            DataRow drw = null;
            drw = dtl.NewRow();

            if (pstrActionType != string.Empty)
            {
                if (dtl.Columns.Contains(m_strActionType))
                    drw[m_strActionType] = pstrActionType;
            }

            if (pstrSearchText != string.Empty)
            {
                try
                {
                    if (dtl.Columns.Contains(pstrSearchColumn))
                        drw[pstrSearchColumn] = pstrSearchText;
                }
                catch (Exception ex)
                {
                    Debug.Print("Error Information " + ex.Message);
                    return;
                }
            }
            dtl.Rows.Add(drw);
            dtl.AcceptChanges();
        }


        /// <summary>
        /// Added by Systems Limited For LOV EXISTS
        /// </summary>
        /// <param name="dtl"></param>
        /// <param name="pstrActionType"></param>
        /// <param name="pstrSearchText"></param>
        /// <param name="pstrSearchColumn"></param>
        public virtual void PopulateDataTableFromObject(DataTable dtl, string pstrActionType, string pstrSearchText, string pstrSearchColumn, string pstrLOVText, string pstrLOVColumn)
        {
            DataRow drw = null;
            drw = dtl.NewRow();

            if (pstrActionType != string.Empty)
            {
                if (dtl.Columns.Contains(m_strActionType))
                    drw[m_strActionType] = pstrActionType;
            }

            if (pstrSearchText != string.Empty)
            {
                if (dtl.Columns.Contains(pstrSearchColumn))
                    drw[pstrSearchColumn] = pstrSearchText;
            }

            if (pstrLOVText != string.Empty)
            {
                if (dtl.Columns.Contains(pstrLOVColumn))
                    drw[pstrLOVColumn] = pstrLOVText;
            }
            dtl.Rows.Add(drw);
            dtl.AcceptChanges();
        }

        /// <summary>
        /// Added by Systems Limited For LOV
        /// </summary>
        /// <param name="pstrSPName"></param>
        /// <param name="pstrActionType"></param>
        /// <param name="pstrSearchText"></param>
        /// <param name="pstrSearchColumn"></param>
        /// <param name="searchFilter"></param>
        /// <returns></returns>
        public Result GetAll(string pstrSPName, string pstrActionType, string pstrSearchText, string pstrSearchColumn, string searchFilter)
        {
            DataSet dst = new DataSet();

            this.spName = pstrSPName;
            dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            this.PopulateDataTableFromObject(dst.Tables[this.spName], pstrActionType, pstrSearchText, pstrSearchColumn);
            if (dst.Tables[this.spName].Columns.Contains("SearchFilter"))
                dst.Tables[this.spName].Rows[0]["SearchFilter"] = searchFilter;

            return SQLManager.SelectDataSet(dst);
        }

        /// <summary>
        /// Added by Systems Limited For LOV
        /// </summary>
        /// <param name="pstrSPName"></param>
        /// <param name="pstrActionType"></param>
        /// <param name="pstrSearchText"></param>
        /// <param name="pstrSearchColumn"></param>
        /// <param name="searchFilter"></param>
        /// <returns></returns>
        public Result GetAll(string pstrSPName, string pstrActionType, string pstrSearchText, string pstrSearchColumn)
        {
            DataSet dst = new DataSet();

            this.spName = pstrSPName;
            dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            this.PopulateDataTableFromObject(dst.Tables[this.spName], pstrActionType, pstrSearchText, pstrSearchColumn);
            return SQLManager.SelectDataSet(dst);
        }

        /// <summary>
        /// Added by Systems Limited For LOV EXISTS
        /// </summary>
        /// <param name="pstrSPName"></param>
        /// <param name="pstrActionType"></param>
        /// <param name="pstrSearchText"></param>
        /// <param name="pstrSearchColumn"></param>
        /// <returns></returns>
        public Result GetAll(string pstrSPName, string pstrActionType, string pstrSearchText, string pstrSearchColumn, string pstrLOVText, string pstrLOVColumn)
        {
            DataSet dst = new DataSet();

            this.spName = pstrSPName;
            dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            this.PopulateDataTableFromObject(dst.Tables[this.spName], pstrActionType, pstrSearchText, pstrSearchColumn, pstrLOVText, pstrLOVColumn);
            return SQLManager.SelectDataSet(dst);
        }

        // Added by SL
        /// <summary>
        /// GetAll
        /// </summary>
        /// <param name="poEntity"></param>
        /// <param name="pstrSPName"></param>
        /// <param name="pstrActionType"></param>
        /// <returns></returns>
        public Result GetAll(BusinessEntity poEntity, string pstrSPName, string pstrActionType)
        {
            DataSet dst = new DataSet();

            this.spName = pstrSPName;
            dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            this.PopulateDataTableFromObject(poEntity, dst.Tables[this.spName], pstrActionType);

            return SQLManager.SelectDataSet(dst);
        }

        // Added by SL
        /// <summary>
        /// GetAll
        /// </summary>
        /// <param name="poEntity"></param>
        /// <param name="pstrSPName"></param>
        /// <param name="pstrActionType"></param>
        /// <returns></returns>
        public Result GetAll(BusinessEntity poEntity, string pstrSPName, string userId, string pstrActionType)
        {
            DataSet dst = new DataSet();

            this.spName = pstrSPName;
            dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            this.PopulateDataTableFromObject(poEntity, dst.Tables[this.spName], pstrActionType);

            return SQLManager.SelectDataSet(dst);
        }

        // Added by SL 
        /// <summary>
        /// GetAll
        /// </summary>
        /// <param name="poEntity"></param>
        /// <param name="pstrSPName"></param>
        /// <param name="pstrActionType"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public Result GetAll(BusinessEntity poEntity, string pstrSPName, string pstrActionType, ref SqlTransaction trans)
        {
            DataSet dst = new DataSet();

            this.spName = pstrSPName;
            dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            this.PopulateDataTableFromObject(poEntity, dst.Tables[this.spName], pstrActionType);

            return SQLManager.SelectDataSet(dst, ref trans);
        }
        /// <summary>
        /// GetAll
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Result GetAll(IEntityCommand entity)
        {
            DataSet dst = new DataSet();

            //this.SetSPName(OperationType.GetAllByParams);

            //dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            //this.PopulateDataTableFromObjectForSelectAll(entity, dst.Tables[this.spName]);

            return SQLManager.SelectDataSet(dst);
        }
        /// <summary>
        /// GetAll
        /// </summary>
        /// <returns></returns>
        public Result GetAll()
        {
            this.SetSPName(OperationType.GetAll);

            return SQLManager.SelectDataSet(this.spName);

        }
        /// <summary>
        /// GetAllAuthorized
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public Result GetAllAuthorized(params object[] values)
        {
            DataSet dst = new DataSet();
            DataRow drw = null;

            this.SetSPName(OperationType.GetAllAuthorized);

            dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            drw = dst.Tables[0].NewRow();

            if (values == null || values.Length == 0)
            {
                for (int i = 0; i < dst.Tables[0].Columns.Count; i++)
                {
                    drw[i] = DBNull.Value;
                }
            }
            else
            {
                for (int i = 0; i < dst.Tables[0].Columns.Count; i++)
                {
                    drw[i] = values[i];
                }
            }

            dst.Tables[0].Rows.Add(drw);
            dst.Tables[0].AcceptChanges();

            return SQLManager.SelectDataSet(dst);
        }
        /// <summary>
        /// GetAllUnAuthorized
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public Result GetAllUnAuthorized(params object[] values)
        {
            DataSet dst = new DataSet();
            DataRow drw = null;

            this.SetSPName(OperationType.GetAllUnAuthorized);

            dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            drw = dst.Tables[0].NewRow();

            if (values == null || values.Length == 0)
            {
                for (int i = 0; i < dst.Tables[0].Columns.Count; i++)
                {
                    drw[i] = DBNull.Value;
                }
            }
            else
            {
                for (int i = 0; i < dst.Tables[0].Columns.Count; i++)
                {
                    drw[i] = values[i];
                }
            }

            dst.Tables[0].Rows.Add(drw);
            dst.Tables[0].AcceptChanges();

            return SQLManager.SelectDataSet(dst);
        }
        /// <summary>
        /// Get
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Result Get(IEntityCommand entity)
        {
            DataSet dst = new DataSet();

            this.SetSPName(OperationType.Get);

            dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            this.PopulateDataTableFromObjectForSelect(entity, dst.Tables[this.spName]);

            return SQLManager.SelectDataSet(dst);
        }
        /// <summary>
        /// GetByCode
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public Result GetByCode(string code)
        {
            DataSet dst = new DataSet();
            DataRow drw = null;

            this.SetSPName(OperationType.GetByCode);

            dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            drw = dst.Tables[0].NewRow();

            drw["Code"] = code;

            dst.Tables[0].Rows.Add(drw);
            dst.Tables[0].AcceptChanges();

            return SQLManager.SelectDataSet(dst);
        }
        /// <summary>
        /// GetByCode
        /// </summary>
        /// <param name="masterId"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public Result GetByCode(int masterId, string code)
        {
            DataSet dst = new DataSet();
            DataRow drw = null;

            this.SetSPName(OperationType.GetByCode);

            dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            drw = dst.Tables[0].NewRow();

            drw["MasterID"] = masterId;
            drw["Code"] = code;

            dst.Tables[0].Rows.Add(drw);
            dst.Tables[0].AcceptChanges();

            return SQLManager.SelectDataSet(dst);
        }
        /// <summary>
        /// IsDuplicate
        /// </summary>
        /// <param name="id"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public Result IsDuplicate(int id, string code)
        {
            DataTable dtl = new DataTable();
            DataRow drw = null;

            this.SetSPName(OperationType.Duplicate);

            dtl = SQLManager.GetSPParams(this.spName);

            drw = dtl.NewRow();

            drw["ID"] = id;
            drw["Code"] = code;

            dtl.Rows.Add(drw);
            dtl.AcceptChanges();

            return SQLManager.SelectScaler(dtl);
        }
        /// <summary>
        /// Authorize
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userId"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        public Result Authorize(int id, string userId, DateTime time)
        {
            DataSet dst = new DataSet();
            DataRow drw = null;

            this.SetSPName(OperationType.Authorize);

            dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            drw = dst.Tables[this.spName].NewRow();

            drw["ID"] = id;
            drw["A_UserID"] = userId;
            drw["A_DateTime"] = time;

            dst.Tables[this.spName].Rows.Add(drw);
            dst.Tables[this.spName].AcceptChanges();

            return SQLManager.ExecuteDML(dst);
        }
        /// <summary>
        /// UnAuthorize
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Result UnAuthorize(int id)
        {
            DataSet dst = new DataSet();
            DataRow drw = null;
            Result rslt;

            this.SetSPName(OperationType.UnAuthorize);

            //dst.Tables.Add(SQLManager.GetSPParams(StoredProcedureList.SP_PREFIX + StoredProcedureList.DEPENDENCIES_GETALL));

            //drw = dst.Tables[0].NewRow();

            //drw["TableName"] = SQLManager.GetTableName(this.spName);
            //drw["PKValue"] = id;

            //dst.Tables[0].Rows.Add(drw);
            //dst.Tables[0].AcceptChanges();

            //rslt = SQLManager.SelectScaler(dst.Tables[0]);

            //if (Convert.ToInt32(rslt.objResult) == 0)
            //{
            dst.Tables.Clear();
            dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            drw = dst.Tables[this.spName].NewRow();

            drw["ID"] = id;

            dst.Tables[this.spName].Rows.Add(drw);
            dst.Tables[this.spName].AcceptChanges();

            rslt = SQLManager.ExecuteDML(dst);
            //}
            //else
            //    rslt.isSuccessful = false;


            return rslt;
        }
        /// <summary>
        /// IsDuplicate
        /// </summary>
        /// <param name="masterId"></param>
        /// <param name="id"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public Result IsDuplicate(int masterId, int id, string code)
        {
            DataTable dtl = new DataTable();
            DataRow drw = null;

            this.SetSPName(OperationType.Duplicate);

            dtl = SQLManager.GetSPParams(this.spName);

            drw = dtl.NewRow();

            drw["ID"] = id;
            drw["MasterID"] = masterId;
            drw["Code"] = code;

            dtl.Rows.Add(drw);
            dtl.AcceptChanges();

            return SQLManager.SelectScaler(dtl);
        }
        /// <summary>
        /// GetStructure
        /// </summary>
        /// <param name="oprtnType"></param>
        /// <returns></returns>
        public DataTable GetStructure(OperationType oprtnType)
        {
            DataTable dtl = new DataTable();

            this.SetSPName(oprtnType);

            return SQLManager.GetSPParams(this.spName);
        }
        /// <summary>
        /// IsNameDuplicate
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public Result IsNameDuplicate(int id, string name)
        {
            DataTable dtl = new DataTable();
            DataRow drw = null;

            this.SetSPName(OperationType.NameDuplicate);

            dtl = SQLManager.GetSPParams(this.spName);

            drw = dtl.NewRow();

            drw["ID"] = id;
            drw["Name"] = name;

            dtl.Rows.Add(drw);
            dtl.AcceptChanges();

            return SQLManager.SelectScaler(dtl);
        }
        /// <summary>
        /// IsNameDuplicate
        /// </summary>
        /// <param name="masterId"></param>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public Result IsNameDuplicate(int masterId, int id, string name)
        {
            DataTable dtl = new DataTable();
            DataRow drw = null;

            this.SetSPName(OperationType.NameDuplicate);

            dtl = SQLManager.GetSPParams(this.spName);

            drw = dtl.NewRow();

            drw["ID"] = id;
            drw["MasterID"] = masterId;
            drw["Name"] = name;

            dtl.Rows.Add(drw);
            dtl.AcceptChanges();

            return SQLManager.SelectScaler(dtl);
        }

        //protected internal abstract IEntityCommand PopulateEntityFromDataTable(DataTable dtl);
        //protected internal abstract void PopulateDataTableFromObject(IEntityCommand entity, DataTable dtl);
        //protected internal abstract void PopulateDataTableFromObjectForDeletion(IEntityCommand entity, DataTable dtl);
        protected internal abstract void PopulateDataTableFromObjectForSelect(IEntityCommand entity, DataTable dtl);
        //protected internal abstract void PopulateDataTableFromObjectForSelectAll(IEntityCommand entity, DataTable dtl);
        protected internal abstract void SetSPName(OperationType oprtnType);

        #region --New Methods--

        /// <summary>
        /// Method to perform Batch/Bulk update for the Grid.
        /// </summary>
        /// <param name="entity">Entity name corresponding to the grid.</param>
        /// <param name="pstrSPName">Manager SP.</param>
        /// <param name="poDT">DT containing the changeset records.</param>
        /// <returns></returns>
        public virtual Result BatchUpdate(IEntityCommand entity, string pstrSPName, DataTable poDT)
        {
            DataSet dst = new DataSet();
            this.spName = pstrSPName;

            dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            return SQLManager.ExecuteBatchUpdate(dst, poDT);
        }

        #endregion

       

    }
}
