﻿namespace CHRIS_REPORTING.Common
{
    public class Constants
    {
        public static class Config
        {
            public const string baseUrl = "";
            public const string branchCode = "branchCode";
            public const string cityCode = "cityCode";
        }
        public static class ResponseCode
        {
            public  const string success = "0";
            public const string error = "-1";
        }
    }
}
