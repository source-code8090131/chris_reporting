﻿using System;

namespace CHRIS_REPORTING.Common
{
    public static class BaseFunction
    {
        public static bool IsValidExpression(String testValue, String regexPattern)
        {
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(regexPattern);
            System.Text.RegularExpressions.MatchCollection matchCollection;
            matchCollection = regex.Matches(testValue);
            if (matchCollection.Count > 0)
                return (matchCollection[0].ToString().Equals(testValue));
            else
                return false;
        }
    }
}
