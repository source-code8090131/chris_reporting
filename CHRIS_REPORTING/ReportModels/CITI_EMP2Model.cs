﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHRIS_REPORTING.ReportModels
{
    public class CITI_EMP2Model
    {
        public string BRN { get; set; }
        public string SPNO { get; set; }
        public string TPNO { get; set; }
        public string SEG { get; set; }
        public string DPT { get; set; }
        public string LEV { get; set; }
        public string P_DESIG { get; set; }
        public string P_GRP { get; set; }
    }
}