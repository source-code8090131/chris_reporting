﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHRIS_REPORTING.ReportModels
{
    public class PY003Model
    {
        public string W_SEG { get; set; }
        public string BRANCH { get; set; }
        public int? W_MONTH { get; set; }
        public int? WYEAR { get; set; }
        public string CAT { get; set; }
    }
}