﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHRIS_REPORTING.ReportModels
{
    public class PRREP03Model
    {
        public string W_BRN { get; set; }
        public string w_seg { get; set; }
        public string w_desig { get; set; }
        public string w_level { get; set; }
    }
}