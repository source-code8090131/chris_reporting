﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHRIS_REPORTING.ReportModels
{
    public class PRREP46Model
    {
        public string BRANCH { get; set; }
        public DateTime? frm_date { get; set; }
        public DateTime? to_date { get; set; }
        public string w_desig { get; set; }
        public string w_seg { get; set; }
        public string pno_start { get; set; }
        public string pno_end { get; set; }
    }
}