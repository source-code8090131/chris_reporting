﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHRIS_REPORTING.ReportModels
{
    public class FNREP09Model
    {
        public string branch { get; set; }
        public string w_seg { get; set; }
        public DateTime? fdate { get; set; }
        public DateTime? tdate { get; set; }
        public string IO_FLAG { get; set; }
    }
}