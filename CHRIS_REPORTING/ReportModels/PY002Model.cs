﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHRIS_REPORTING.ReportModels
{
    public class PY002Model
    {
        public string W_BRANCH { get; set; }
        public int? W_MONTH { get; set; }
        public int? YEAR { get; set; }
        public string c_flag { get; set; }
        public string cat { get; set; }
        public string W_SEG { get; set; }
    }
}