﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHRIS_REPORTING.ReportModels
{
    public class FNREP01Model
    {
        public string w_branch { get; set; }
        public DateTime? wmdate { get; set; }
        public string seg { get; set; }
    }
}