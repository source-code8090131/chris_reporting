﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHRIS_REPORTING.ReportModels
{
    public class ATR_INCENTIVE_ALLOWModel
    {
        public string P_BRANCH { get; set; }
        public string P_CAT { get; set; }
        public string P_ALLOW { get; set; }
        public int? P_EMP_NO { get; set; }
    }
}