﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHRIS_REPORTING.ReportModels
{
    public class GRR02Model
    {
        public int? w_pfrom { get; set; }
        public int? w_pto { get; set; }
        public string w_seg { get; set; }
        public string w_brn { get; set; }
        public string w_dept { get; set; }
    }
}