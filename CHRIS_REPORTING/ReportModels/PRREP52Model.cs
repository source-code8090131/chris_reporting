﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHRIS_REPORTING.ReportModels
{
    public class PRREP52Model
    {
        public string w_brn { get; set; }
        public string w_seg { get; set; }
        public string w_group { get; set; }
        public DateTime? sdate { get; set; }
        public DateTime? edate { get; set; }
    }
}