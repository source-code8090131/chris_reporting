﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHRIS_REPORTING.ReportModels
{
    public class MCOREP2Model
    {
        public decimal? GL_BALANCE { get; set; }
        public string W_BRANCH { get; set; }
        public string W_SEG { get; set; }
    }
}