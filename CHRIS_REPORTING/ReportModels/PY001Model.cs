﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHRIS_REPORTING.ReportModels
{
    public class PY001Model
    {
        public string w_brn { get; set; }
        public DateTime? date1 { get; set; }
        public DateTime? date2 { get; set; }
    }
}