﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHRIS_REPORTING.ReportModels
{
    public class PRREP06Model
    {
        public DateTime? S_DATE { get; set; }
        public DateTime? E_DATE { get; set; }
        public string W_SEG { get; set; }
        public string W_BRN { get; set; }
        public string W_DESIG { get; set; }
        public string W_GROUP { get; set; }
    }
}