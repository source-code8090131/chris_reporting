﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHRIS_REPORTING.ReportModels
{
    public class PY011_BModel
    {
        public string BRANCH { get; set; }
        public string MODE { get; set; }
        public string W_SEG { get; set; }
        public string W_DEPT { get; set; }
        public string CAT { get; set; }
        public string FPNO { get; set; }
        public string TPNO { get; set; }
        public int? MONTH { get; set; }
        public int? WYEAR { get; set; }
    }
}