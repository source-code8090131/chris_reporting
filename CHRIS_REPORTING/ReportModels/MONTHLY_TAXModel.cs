﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHRIS_REPORTING.ReportModels
{
    public class MONTHLY_TAXModel
    {
        public string p_branch { get; set; }
        public DateTime? p_month { get; set; }
    }
}